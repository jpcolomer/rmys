class CreateSongs < ActiveRecord::Migration
  def change
    create_table :songs do |t|
      t.string :name
      t.integer :artist_id

      t.timestamps
    end
    add_index :songs, :artist_id
    add_index :songs, :name
  end
end
