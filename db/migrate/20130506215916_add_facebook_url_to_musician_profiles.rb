class AddFacebookUrlToMusicianProfiles < ActiveRecord::Migration
  def change
    add_column :musician_profiles, :facebook_url, :string
  end
end
