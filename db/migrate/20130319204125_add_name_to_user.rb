class AddNameToUser < ActiveRecord::Migration
  def up
    add_column :users, :name, :string
    add_index :users, :name
  end
  def down
    remove_index :users, :name
    remove_column :users, :name
  end
end
