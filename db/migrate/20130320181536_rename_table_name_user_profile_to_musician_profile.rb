class RenameTableNameUserProfileToMusicianProfile < ActiveRecord::Migration
  def up
    rename_table :user_profiles, :musician_profiles
    rename_column :concerts, :user_profile_id, :musician_profile_id
  end

  def down
    rename_table :musician_profiles, :user_profiles
    rename_column :concerts, :musician_profile_id, :user_profile_id
  end
end
