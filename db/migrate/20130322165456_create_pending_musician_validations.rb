class CreatePendingMusicianValidations < ActiveRecord::Migration
  def change
    create_table :pending_musician_validations do |t|
      t.integer :musician_profile_id

      t.timestamps
    end
    add_index :pending_musician_validations, :musician_profile_id
  end
end
