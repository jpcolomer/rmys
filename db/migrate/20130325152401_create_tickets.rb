class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :user_id
      t.integer :concert_id

      t.timestamps
    end
    add_index :tickets, [:user_id, :concert_id]
  end
end
