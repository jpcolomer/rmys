class RemoveArtistIdFromGenres < ActiveRecord::Migration
  def up
    remove_index :genres, :artist_id
    remove_column :genres, :artist_id
  end

  def down
    add_column :genres, :artist_id, :integer
    add_index :genres, :artist_id
  end
end
