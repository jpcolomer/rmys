class ChangeDefaultValuePerformedInConcerts < ActiveRecord::Migration
  def up
    change_column :concerts, :performed, :boolean, default: false, null: false
  end

  def down
    change_column :concerts, :performed, :boolean
  end
end
