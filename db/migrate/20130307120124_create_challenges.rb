class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.integer :user_id
      t.integer :song_id
      t.timestamps
    end
    add_index :challenges, :user_id
    add_index :challenges, :song_id
  end
end
