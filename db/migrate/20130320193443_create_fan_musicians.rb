class CreateFanMusicians < ActiveRecord::Migration
  def change
    create_table :fan_musicians do |t|
      t.integer :fan_id
      t.integer :musician_id

      t.timestamps
    end
    add_index :fan_musicians, [:fan_id, :musician_id]
  end
end
