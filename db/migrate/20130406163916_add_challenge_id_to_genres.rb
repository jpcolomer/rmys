class AddChallengeIdToGenres < ActiveRecord::Migration
  def change
    add_column :genres, :challenge_id, :integer
    add_index :genres, :challenge_id
  end
end
