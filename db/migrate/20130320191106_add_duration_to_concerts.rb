class AddDurationToConcerts < ActiveRecord::Migration
  def change
    add_column :concerts, :duration, :integer
  end
end
