class AddDonationToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :donation, :integer
  end
end
