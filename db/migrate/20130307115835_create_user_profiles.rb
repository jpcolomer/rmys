class CreateUserProfiles < ActiveRecord::Migration
  def change
    create_table :user_profiles do |t|
      t.string :name
      t.text :edito
      t.string :photo
      t.string :twitter_url
      t.string :facebook_url
      t.integer :user_id

      t.timestamps
    end
    add_index :user_profiles, :user_id
  end
end
