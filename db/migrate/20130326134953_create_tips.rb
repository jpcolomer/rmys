class CreateTips < ActiveRecord::Migration
  def change
    create_table :tips do |t|
      t.integer :user_id
      t.integer :musician_profile_id
      t.integer :amount

      t.timestamps
    end
    add_index :tips, [:user_id, :musician_profile_id]
  end
end
