class CreateConcerts < ActiveRecord::Migration
  def change
    create_table :concerts do |t|
      t.string :name
      t.datetime :date
      t.text :description
      t.integer :ticket_price
      t.boolean :performed
      t.integer :user_profile_id
      t.integer :challenge_id

      t.timestamps
    end
  end
end
