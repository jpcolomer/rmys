class AddValidationToMusicianProfiles < ActiveRecord::Migration
  def change
    add_column :musician_profiles, :validation, :boolean
    add_index :musician_profiles, :validation
  end
end
