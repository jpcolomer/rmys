class AddEndDateToConcerts < ActiveRecord::Migration
  def change
    add_column :concerts, :end_date, :datetime
  end
end
