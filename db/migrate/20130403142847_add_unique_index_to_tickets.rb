class AddUniqueIndexToTickets < ActiveRecord::Migration
  def up
    remove_index :tickets, [:user_id, :concert_id]
    add_index :tickets, [:user_id, :concert_id], unique: true
  end
  def down
    remove_index :tickets, [:user_id, :concert_id]
    add_index :tickets, [:user_id, :concert_id]
  end
end
