class RenameColumnsInMusicianProfile < ActiveRecord::Migration
  def change
    rename_column :musician_profiles, :facebook_url, :youtube_url
    rename_column :musician_profiles, :twitter_url, :other_url
  end
end
