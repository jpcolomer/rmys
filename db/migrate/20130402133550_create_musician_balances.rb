class CreateMusicianBalances < ActiveRecord::Migration
  def change
    create_table :musician_balances do |t|
      t.integer :musician_profile_id
      t.integer :credit, default: 0

      t.timestamps
    end
    add_index :musician_balances, :musician_profile_id
  end
end
