# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130508172236) do

  create_table "admins", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true
  add_index "admins", ["reset_password_token"], :name => "index_admins_on_reset_password_token", :unique => true

  create_table "artists", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "artists", ["name"], :name => "index_artists_on_name"

  create_table "balances", :force => true do |t|
    t.integer  "credit",     :default => 0
    t.integer  "user_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "balances", ["user_id"], :name => "index_balances_on_user_id"

  create_table "challenges", :force => true do |t|
    t.integer  "user_id"
    t.integer  "song_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "challenges", ["song_id"], :name => "index_challenges_on_song_id"
  add_index "challenges", ["user_id"], :name => "index_challenges_on_user_id"

  create_table "concerts", :force => true do |t|
    t.string   "name"
    t.datetime "date"
    t.text     "description"
    t.integer  "ticket_price"
    t.boolean  "performed",           :default => false, :null => false
    t.integer  "musician_profile_id"
    t.integer  "challenge_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.integer  "duration"
    t.datetime "end_date"
  end

  create_table "fan_musicians", :force => true do |t|
    t.integer  "fan_id"
    t.integer  "musician_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "fan_musicians", ["fan_id", "musician_id"], :name => "index_fan_musicians_on_fan_id_and_musician_id"

  create_table "genres", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "challenge_id"
  end

  add_index "genres", ["challenge_id"], :name => "index_genres_on_challenge_id"
  add_index "genres", ["name"], :name => "index_genres_on_name"

  create_table "musician_balances", :force => true do |t|
    t.integer  "musician_profile_id"
    t.integer  "credit",              :default => 0
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "musician_balances", ["musician_profile_id"], :name => "index_musician_balances_on_musician_profile_id"

  create_table "musician_profiles", :force => true do |t|
    t.text     "edito"
    t.string   "photo"
    t.string   "other_url"
    t.string   "youtube_url"
    t.integer  "user_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "name"
    t.boolean  "validation"
    t.string   "facebook_url"
  end

  add_index "musician_profiles", ["user_id"], :name => "index_user_profiles_on_user_id"
  add_index "musician_profiles", ["validation"], :name => "index_musician_profiles_on_validation"

  create_table "payments", :force => true do |t|
    t.integer  "amount"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "payments", ["user_id"], :name => "index_payments_on_user_id"

  create_table "pending_musician_validations", :force => true do |t|
    t.integer  "musician_profile_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "pending_musician_validations", ["musician_profile_id"], :name => "index_pending_musician_validations_on_musician_profile_id"

  create_table "rails_admin_histories", :force => true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      :limit => 2
    t.integer  "year",       :limit => 8
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], :name => "index_rails_admin_histories"

  create_table "songs", :force => true do |t|
    t.string   "name"
    t.integer  "artist_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "songs", ["artist_id"], :name => "index_songs_on_artist_id"
  add_index "songs", ["name"], :name => "index_songs_on_name"

  create_table "tickets", :force => true do |t|
    t.integer  "user_id"
    t.integer  "concert_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "amount"
    t.integer  "donation"
  end

  add_index "tickets", ["user_id", "concert_id"], :name => "index_tickets_on_user_id_and_concert_id", :unique => true

  create_table "tips", :force => true do |t|
    t.integer  "user_id"
    t.integer  "musician_profile_id"
    t.integer  "amount"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "tips", ["user_id", "musician_profile_id"], :name => "index_tips_on_user_id_and_musician_profile_id"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "name"
    t.string   "photo"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["name"], :name => "index_users_on_name"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
