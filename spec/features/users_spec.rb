require'spec_helper'

feature 'User management' do
  after { Warden.test_reset! }

  let(:user) { create(:user) }
  scenario "sign up" do
    visit root_path
    click_link 'Profile'
    click_link 'Sign up'
    within '#sign-up-modal' do
      fill_in "Email", with: "jp@jp.jp"
      fill_in "Name", with: "jp"

      fill_in "Password", with: "12345678"
      fill_in "Password confirmation", with: "12345678"
      expect{ click_button 'Sign up'}.to change{ User.count }.by(1)
    end
    expect(current_path).to eq root_path
  end

  scenario "sign in", js: true do
    Capybara.current_session.driver.browser.manage.window.resize_to(1800, 1024)
    visit root_path
    click_link 'Profile'
    click_link 'Sign in'
    within '#log-in-modal' do
      fill_in "Email", with: user.email
      fill_in "Password", with: user.password
      click_button 'Log in'
    end
    expect(current_path).to eq root_path
  end

end