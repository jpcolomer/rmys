require 'spec_helper'

feature "User gives tip" do

  context "on concert page" do
    
    scenario "with credit gives tip", js: true do
      user = create(:wealthy_user)
      concert = create(:concert)
      log_in_as(user)

      opens_concert_tip_page(concert)
      gives_tip(10)
      expect(current_path).to eq root_path
      user_sees_message "You tipped this musician with 10 credits"
    end


    scenario "with credit gives tip on second concert page", js: true do
      user = create(:wealthy_user)
      concert1 = create(:concert, date: 1.days.from_now)
      concert2 = create(:concert, date: 2.days.from_now)
      log_in_as(user)
      navigate_to_next_concert
      opens_concert_tip_page(concert2)
      gives_tip(10)
      expect(current_path).to eq root_path
      user_sees_message "You tipped this musician with 10 credits"
    end

    scenario "try to give tip without filling amount", js: true do
      user = create(:user)
      concert = create(:concert)
      log_in_as(user)

      opens_concert_tip_page(concert)
      doesnt_change_user_balance(user)

    end

    scenario "cancel purchase", js: true do
      user = create(:user)
      concert = create(:concert)
      sign_in(user)

      opens_concert_tip_page(concert)

      click_button 'Cancel'
      expect(current_path).to eq root_path
    end

  end

  context "on Musician Profile" do
    scenario "with credit", js: true do
      user = create(:wealthy_user)
      musician = create(:valid_musician)
      log_in_as(user)

      opens_musician_tip_page(musician)
      gives_tip(10)
      expect(current_path).to eq musician_profile_path(musician)
      user_sees_message "You tipped this musician with 10 credits"
    end

  end


end