require 'spec_helper'
require 'faker'

feature "Fan Browsing" do
  let(:user) { create(:user) }

  context 'enable javascript' do
    before(:each) do
      Capybara.current_session.driver.browser.manage.window.resize_to(1800, 1024)
    end

    context "without previous sign in" do
      before :each do
        create(:valid_musician, user: user)
      end

      let(:musician) { create(:valid_musician, user: user) }
      context "peform live online" do

        scenario "by clicking navbar link", js: true do
          visit root_path
          click_link 'PERFORM LIVE ONLINE'
          log_in(user)
          expect(current_path).to eq challenges_path
        end

        scenario "by clicking arrow", js: true do
          visit root_path
          find('#perform-link').click
          log_in(user)
          expect(current_path).to eq challenges_path
        end
        
      end

      scenario "redirects to login when send new challenge", js: true do
        visit root_path
        within '#new_challenge' do
          click_link 'SEND'
        end
        log_in(user)
        expect(current_path).to eq root_path
      end

    end

    scenario "looks for a valid musician", js: true do
      musician = create(:valid_musician)
      visit root_path
      within ".navbar-search" do
        fill_in "name", with: musician.name[0..2]
        click_link musician.name
      end
      expect(current_path).to eq musician_profile_path(musician)
    end

    scenario "can't look for an invalid musician", js: true do
      musician = create(:musician_profile)
      visit root_path
      within ".navbar-search" do
        fill_in "name", with: musician.name[0..2]
      end
      expect(page).to_not have_content musician.name
    end

  end


  context "with previous sign in" do
    before(:each) do
      sign_in(user)
      visit root_path
    end

    scenario 'add challenge with one genre' do
      
      expect {
        within '#new_challenge' do 
          fill_in "challenge_song_attributes_artist_attributes_name", with: "U2"
          fill_in "challenge_song_attributes_name", with: "One"
          select 'Pop', from: 'challenge_genres_attributes_0_name'
          click_button 'SEND'
        end
      }.to change{ Challenge.count }.by(1)
    end


    scenario "doesn't saves to the DB with no Genre" do
      expect {
        within '#new_challenge' do 
          fill_in "challenge_song_attributes_artist_attributes_name", with: "U2"
          fill_in "challenge_song_attributes_name", with: "One"
          click_button 'SEND'
        end
      }.to_not change{ Challenge.count }         
    end

    scenario 'add challenge with two genre' do
      
      expect {
        within '#new_challenge' do 
          fill_in "challenge_song_attributes_artist_attributes_name", with: "U2"
          fill_in "challenge_song_attributes_name", with: "One"
          select 'Pop', from: 'challenge_genres_attributes_0_name'
          select 'Rock', from: 'challenge_genres_attributes_0_name'
          click_button 'SEND'
        end
      }.to change{ Challenge.count }.by(1)
    end

  end


  scenario "genre dropdown contains all genres" do
    visit root_path
    within '#new_challenge' do
      Genre::GENRES.each do |genre|
        expect(page).to have_content genre
      end
    end
  end


end