require'spec_helper'
require 'faker'

feature "Musician management" do
  
  context "with valid musician" do
    let(:musician) { create(:valid_musician) }

    scenario "have all buttons and content on musician profile" do
      performed_concert = create(:performed_concert, musician_profile: musician)
      challenge = create(:challenge)
      sign_in(musician.user)
      click_link 'Musician / Band'
      expect(current_path).to eq musician_profile_path(musician)
      expect(page).to have_content musician.name
      expect(page).to have_content musician.edito
      expect(page).to have_content 'Play a Challenge'
      expect(page).to have_content "Balance: 0"
      expect(page).to_not have_content performed_concert.name
      expect(page).to_not have_content 'Become Fan'
      click_link 'Play a Challenge'
      expect(current_path).to eq challenges_path
      pick_challenge(challenge)
      expect(current_path).to eq new_concert_path
      expect{
        fill_concert_form
      }.to change{ Concert.count }.by(1)
      expect(current_path).to eq musician_profile_path(musician)
      expect(page).to_not have_content 'Play a Challenge'
      
      within '[data-role="concert-description"]' do
        expect(page).to have_content musician.valid_concerts.first.name
        expect(page).to have_content musician.valid_concerts.first.pretty_date
        expect(page).to have_css '.btn-rmys', text: 'GO LIVE'
      end

    end

    scenario "meet challenge" do
      challenge = create(:challenge)
      sign_in(musician.user)
      click_link 'PERFORM LIVE ONLINE'
      expect(current_path).to eq challenges_path
      pick_challenge(challenge)
      expect(current_path).to eq new_concert_path
      
      expect{
        fill_concert_form
      }.to change{ Concert.count }.by(1)

      expect(current_path).to eq musician_profile_path(musician)

    end

    scenario "can't have more than 1 concert" do
      create(:concert, musician_profile: musician)
      challenge = create(:challenge)
      sign_in(musician.user)
      click_link 'PERFORM LIVE ONLINE'
      expect(current_path).to eq challenges_path
      pick_challenge(challenge)
      expect(current_path).to eq new_concert_path
      
      expect{
        fill_concert_form
      }.to_not change{ Concert.count }

      expect(page).to have_content "Musician profile already have one concert to be performed"

    end

    context "goes to peform live online" do
      before :each do
        sign_in(musician.user)
      end
      scenario "by clicking navbar link" do
        click_link 'PERFORM LIVE ONLINE'
        expect(current_path).to eq challenges_path
      end

      scenario "by clicking arrow" do
        find('#perform-link').click
        expect(current_path).to eq challenges_path
      end
      
    end

    scenario "sort challenge list by genre" do
      challenge1 = build(:challenge)
      challenge1.genres = [build(:genre, name: 'Pop')]
      challenge1.save
      expect(challenge1.genres_list).to eq 'Pop' 
      challenge2 = build(:challenge)
      challenge2.genres = [build(:genre, name: 'Rock')]
      challenge2.save
      expect(challenge2.genres_list).to eq 'Rock'

      sign_in(musician.user)
      visit challenges_path
      within '#sortable-table' do
        expect(find(:xpath, "//tbody/tr[1]/td[1]")).to have_content 'Pop'
        expect(find(:xpath, "//tbody/tr[2]/td[1]")).to have_content 'Rock'
        expect(find_link('Genre')[:href]).to eq "/challenges?direction=desc&sort=genre"
        click_link 'Genre'
      end

      within '#sortable-table' do
        expect(find_link('Genre')[:href]).to eq "/challenges?direction=asc&sort=genre"
        expect(find(:xpath, "//tbody/tr[1]/td[1]")).to have_content 'Rock'
        expect(find(:xpath, "//tbody/tr[2]/td[1]")).to have_content 'Pop'
      end
    end


    scenario "sort challenge list by song" do
      song1 = create(:song, name: 'Song1')
      challenge1 = create(:challenge, song: song1)
      song2 = create(:song, name: 'Song2')
      challenge2 = create(:challenge, song: song2)

      sign_in(musician.user)
      visit challenges_path
      click_link 'Song'
      within '#sortable-table' do
        expect(find(:xpath, "//tbody/tr[1]/td[3]")).to have_content 'Song1'
        expect(find(:xpath, "//tbody/tr[2]/td[3]")).to have_content 'Song2'
        expect(find_link('Song')[:href]).to eq "/challenges?direction=desc&sort=song"
        click_link 'Song'
      end

      within '#sortable-table' do
        expect(find_link('Song')[:href]).to eq "/challenges?direction=asc&sort=song"
        expect(find(:xpath, "//tbody/tr[1]/td[3]")).to have_content 'Song2'
        expect(find(:xpath, "//tbody/tr[2]/td[3]")).to have_content 'Song1'
      end
    end


    scenario "sort challenge list by artist" do
      artist1 = create(:artist, name: 'Artist1')
      song1 = create(:song, artist: artist1)
      challenge1 = create(:challenge, song: song1)
      artist2 = create(:artist, name: 'Artist2')
      song2 = create(:song, artist: artist2)
      challenge2 = create(:challenge, song: song2)

      sign_in(musician.user)
      visit challenges_path
      click_link 'Artist'
      within '#sortable-table' do
        expect(find(:xpath, "//tbody/tr[1]/td[2]")).to have_content 'Artist1'
        expect(find(:xpath, "//tbody/tr[2]/td[2]")).to have_content 'Artist2'
        expect(find_link('Artist')[:href]).to eq "/challenges?direction=desc&sort=artist"
        click_link 'Artist'
      end

      within '#sortable-table' do
        expect(find_link('Artist')[:href]).to eq "/challenges?direction=asc&sort=artist"
        expect(find(:xpath, "//tbody/tr[1]/td[2]")).to have_content 'Artist2'
        expect(find(:xpath, "//tbody/tr[2]/td[2]")).to have_content 'Artist1'
      end
    end



  end

  context "with invalid musician" do
    let(:musician) { create(:invalid_musician) }
    scenario "can't meet challenge" do
      challenge = create(:challenge)
      sign_in(musician.user)
      click_link 'PERFORM LIVE ONLINE'
      expect(current_path).to eq challenges_path
      pick_challenge(challenge)
      expect(current_path).to eq musician_profile_path(musician)
      expect(page).to have_content "Your musician profile is not valid or hasn't been validated yet"
    end   
  end


  context "with pending valid musician" do
    let(:musician) { create(:musician_profile) }
    scenario "can't meet challenge" do
      challenge = create(:challenge)
      sign_in(musician.user)
      click_link 'PERFORM LIVE ONLINE'
      expect(current_path).to eq challenges_path
      pick_challenge(challenge)
      expect(current_path).to eq musician_profile_path(musician)
      expect(page).to have_content "Your musician profile is not valid or hasn't been validated yet"
    end   
  end

  scenario "has link to youtube, facebook and twitter" do
    musician = create(:musician_profile, facebook_url: 'http://www.facebook.com/pages/Rockmystage', youtube_url: 'http://www.youtube.com/watch?v=GUULNJEdKU8',
      other_url: 'https://twitter.com/RockMyStagecom')
      visit musician_profile_path(musician)
      expect(find('#youtube-url')[:href]).to eq 'http://www.youtube.com/watch?v=GUULNJEdKU8'
      expect(find('#facebook-url')[:href]).to eq 'http://www.facebook.com/pages/Rockmystage'
      expect(find('#other-url')[:href]).to eq 'https://twitter.com/RockMyStagecom'
      within "#youtube-url" do
        expect(find('img')[:src]).to eq "/assets/youtube.com.png"
      end
      within "#facebook-url" do
        expect(find('img')[:src]).to eq "/assets/facebook.com.png"
      end
      within "#other-url" do
        expect(find('img')[:src]).to eq "/assets/twitter.com.png"
      end
  end

  scenario "has link to soundcloud " do
    musician = create(:musician_profile, other_url: 'https://soundcloud.com/avrcrc')
      visit musician_profile_path(musician)

      expect(find('#other-url')[:href]).to eq 'https://soundcloud.com/avrcrc'

      within "#other-url" do
        expect(find('img')[:src]).to eq "/assets/soundcloud.com.png"
      end
  end

  scenario "has link to myspace " do
    musician = create(:musician_profile, other_url: 'https://new.myspace.com/blackveilbrides/music/songs')
      visit musician_profile_path(musician)

      expect(find('#other-url')[:href]).to eq 'https://new.myspace.com/blackveilbrides/music/songs'

      within "#other-url" do
        expect(find('img')[:src]).to eq "/assets/new.myspace.com.png"
      end
  end

  scenario "has link to itunes " do
    musician = create(:musician_profile, other_url: 'https://itunes.apple.com/us/artist/the-killers/id6483093')
      visit musician_profile_path(musician)

      expect(find('#other-url')[:href]).to eq 'https://itunes.apple.com/us/artist/the-killers/id6483093'

      within "#other-url" do
        expect(find('img')[:src]).to eq "/assets/itunes.apple.com.png"
      end
  end


  scenario "has no social media link" do
    musician = create(:musician_profile)
    visit musician_profile_path(musician)

    expect(page).to have_no_selector '#youtube-url'
    expect(page).to have_no_selector '#facebook-url'
    expect(page).to have_no_selector '#other-url'

  end


end