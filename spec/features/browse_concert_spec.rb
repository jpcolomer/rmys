require 'spec_helper'

feature "User browses future concerts", js: true do
  scenario "show next and previous concert" do
    first_concert = create(:concert, date: 1.days.from_now)
    second_concert = create(:concert, date: 2.days.from_now)
    visit root_path

    click_link first_concert.musician.name
    expect(current_path).to eq musician_profile_path(first_concert.musician)
    page.evaluate_script('window.history.back()')
    expect(current_path).to eq root_path

    within '#show-concert' do
      expect(page).to have_no_css 'div.previous', visible: false
      expect(page).to have_css 'div.next', visible: false
      expect(page).to have_content first_concert.description
      click_link first_concert.musician.name
    end

    expect(current_path).to eq musician_profile_path(first_concert.musician)
    page.evaluate_script('window.history.back()')
    expect(current_path).to eq root_path

    find('.next-wrapper').click
    
    within '#show-concert' do
      expect(page).to have_css 'div.previous', visible: false
      expect(page).to have_no_css 'div.next', visible: false
      expect(page).to have_content second_concert.description
      expect(page).to have_content second_concert.musician
      click_link second_concert.musician.name
      expect(current_path).to eq musician_profile_path(second_concert.musician)
    end
  end

  scenario "by clicking arrows", js: true do
    concert1 = create(:concert, date: 1.days.from_now)
    concert2 = create(:concert, date: 2.days.from_now)
    visit root_path

    within "#concert_#{concert1.id}" do
      expect(page).to have_content concert1.pretty_date
      expect(page).to have_content concert1.countdown
      expect(page).to have_content concert1.challenge.song_name
      expect(page).to have_content concert1.challenge.artist_name
      expect(page).to have_content concert1.duration
    end
    find('.next-wrapper').click

    within "#concert_#{concert2.id}" do
      expect(page).to have_content concert2.pretty_date
      expect(page).to have_content concert2.countdown
      expect(page).to have_content concert2.challenge.song_name
      expect(page).to have_content concert2.challenge.artist_name
      expect(page).to have_content concert2.duration
    end
    find('.previous-wrapper').click
    within "#concert_#{concert1.id}" do
      expect(page).to have_content concert1.pretty_date
      expect(page).to have_content concert1.countdown
      expect(page).to have_content concert1.challenge.song_name
      expect(page).to have_content concert1.challenge.artist_name
      expect(page).to have_content concert1.duration
    end
  end


end