require 'spec_helper'

feature "User buys ticket" do

  scenario "with enough credit for first concert", js: true do
    user = create(:wealthy_user)
    concert = create(:concert, date: 1.days.from_now)
    log_in_as(user)

    open_buy_concert_ticket(concert)

    expect(current_path).to eq root_path
    give_donation 7
    buy_ticket
    
    expect(current_path).to eq root_path

    user_sees_message "Ticket bought for concert #{concert.name}"
    user_doesnt_see 'BUY TICKET'
  end

  scenario "with enough credit for second concert", js: true do
    user = create(:wealthy_user)
    concert1 = create(:concert, date: 1.days.from_now)
    concert2 = create(:concert, date: 2.days.from_now)
    log_in_as(user)
    
    navigate_to_next_concert

    open_buy_concert_ticket(concert2)

    give_donation 5
    buy_ticket

    # musician_balance_increases_by(concert2.musician, 15)   

    expect(current_path).to eq root_path
    user_sees_message "Ticket bought for concert #{concert2.name}"
    navigate_to_next_concert
    user_doesnt_see 'BUY TICKET'
  end

  scenario "cancel purchase", js: true do
    user = create(:user)
    concert = create(:concert, date: 1.days.from_now)
    log_in_as(user)

    open_buy_concert_ticket(concert)
    click_button 'Cancel'
    expect(current_path).to eq root_path
  end

  scenario 'ask for login if there is no user logged', js: true do
    user = create(:user)
    concert = create(:concert, date: 1.days.from_now)
    concert2 = create(:concert, date: 2.days.from_now)
    visit root_path
    open_buy_concert_ticket(concert)
    log_in(user)
    open_buy_concert_ticket(concert)
  end


end