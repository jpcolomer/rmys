require 'spec_helper'

feature "Concert shows on Musician profile" do
  scenario "pass the time and he doesn't perform it" do
    musician = create(:valid_musician)
    concert = create(:concert, musician_profile: musician)
    visit musician_profile_path(musician)
    expect(page).to have_css '[data-role="concert-description"]'
    concert.date = 10.days.ago
    concert.save!
    visit musician_profile_path(musician)
    expect(page).to have_css '[data-role="concert-description"]'
  end
end