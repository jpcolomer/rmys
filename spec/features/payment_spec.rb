require 'spec_helper'

feature "User buy credits" do
  scenario "from his fan profile", js: true do
    user = create(:user, name: 'test')
    log_in_as(user)

    goes_to_user_profile(user, 'Test')

    opens_payment_page
    expect(current_path).to eq user_path(user)
    buy_credits(10)
    expect(current_path).to eq user_path(user)
    user_sees_message 'Balance: 10'

  end

  scenario "when wants to buy ticket", js: true do
    user = create(:user)
    concert = create(:concert, date: 1.days.from_now)
    log_in_as(user)

    open_buy_concert_ticket(concert)
    buy_ticket
    expect(current_path).to eq root_path
    buy_credits(10)
    expect(current_path).to eq root_path
    user_sees_message "You bought 10 credits"
  end


  scenario "when wants to buy ticket, credits >= 10 and ticket + donation > credits", js: true do
    user = create(:user)
    user.increase_credit(10)
    user.reload
    concert = create(:concert, date: 1.days.from_now)
    log_in_as(user)

    open_buy_concert_ticket(concert)
    give_donation(5)
    buy_ticket
    expect(current_path).to eq root_path
    buy_credits(10)
    expect(current_path).to eq root_path
    user_sees_message "You bought 10 credits"
  end


  scenario "when wants to tip a musician from concert page", js: true do
    user = create(:user)
    concert = create(:concert, date: 1.days.from_now)
    log_in_as(user)
    opens_concert_tip_page(concert)
    gives_tip(10)
    expect(current_path).to eq root_path
    buy_credits(10)
    expect(current_path).to eq root_path
    user_sees_message "You bought 10 credits"
  end


  scenario "when wants to tip a musician from musician page", js: true do
    user = create(:user)
    musician = create(:valid_musician)
    log_in_as(user)

    opens_musician_tip_page(musician)
    gives_tip(10)
    expect(current_path).to eq musician_profile_path(musician)
    buy_credits(10)
    expect(current_path).to eq musician_profile_path(musician)
    user_sees_message "You bought 10 credits"
  end

end