require 'spec_helper'

feature "User hit watch concert" do
  let(:user) { create(:wealthy_user) }

  scenario "redirects to buy ticket without one", js: true do
    concert = create(:concert)
    sign_in(user)
    within "#concert_#{concert.id}" do
      click_link 'PLAY'
    end
    expect(page).to have_content "You need to buy a ticket to watch this concert"
    expect(current_path).to eq root_path
  end


  scenario "asks for login before look at concert broadcast", js: true do
    create(:concert)
    visit root_path

    within ".play-wrapper" do
      click_link "PLAY"
    end
    log_in(user)

  end



  context "with ticket" do

    scenario "concert.date < 10min from now, send wait message", js: true do
      concert = create(:concert, date: 13.minutes.from_now)
      create(:ticket, concert: concert, user: user)
      sign_in(user)
      click_link 'PLAY'
      expect(current_path).to_not eq concert_broadcast_path(concert)
      expect(current_path).to eq root_path
      expect(page).to have_content 'Concert did not start yet! Please click on Play 10 min before'
    end

    scenario "concert.date > 10min from now, goes to broadcast", js: true do
      concert = create(:concert, date: 9.minutes.from_now)
      create(:ticket, concert: concert, user: user)
      sign_in(user)
      click_link 'PLAY'
      expect(current_path).to eq root_path
      expect(page).to have_css '[data-role="concert-broadcast"]'
    end 

    scenario "concert.date + duration < now, show concert", js: true do
      concert = create(:concert, date: 1.minutes.from_now, duration: 30)
      concert.date = 10.minutes.ago 
      concert.save
      create(:ticket, concert: concert, user: user)
      sign_in(user)
      click_link 'PLAY'
      expect(current_path).to eq root_path
      expect(page).to have_css '[data-role="concert-broadcast"]'
    end 
  end

  context "musician and user are the same" do
    scenario "can watch himself when is show time", js: true do
      musician = create(:valid_musician)
      concert = create(:concert, date: 8.minutes.from_now, musician_profile: musician)
      sign_in(musician.user)
      click_link 'PLAY'
      expect(current_path).to eq root_path
      expect(page).to have_css '[data-role="concert-broadcast"]'
    end
  end

end