# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ticket do
    concert
    user
    donation { rand(1..10) }

    factory :ticket_with_wealthy_user do
      association :user, factory: :wealthy_user
    end

  end
end
