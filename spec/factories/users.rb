require 'faker'

FactoryGirl.define do 
  factory :user do
    name { Faker::Internet.user_name }
    email { Faker::Internet.email }
    password '12345678'

    factory :wealthy_user do
      after(:create) do |user|
        user.increase_credit(1000)
      end
    end
  end 
end