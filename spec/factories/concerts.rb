# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :concert do
    challenge
    musician_profile
    description { Faker::Lorem.paragraph(3) }
    sequence(:name) {|n| "Concert#{n}" }
    duration { [10, 30].sample }
    date { rand(1..20).days.from_now }
    performed false

    factory :performed_concert do
      performed true
      after(:create) do |concert|
        concert.date = rand(1..20).days.ago
      end
    end
  end
end
