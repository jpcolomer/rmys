# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :tip do
    musician_profile
    user
    amount { rand(1..10) }

    factory :tip_with_user_credit do
      association :user, factory: :wealthy_user
    end
  end

end
