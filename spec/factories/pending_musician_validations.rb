# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :pending_musician_validation do
    musician_profile strategy: :build
  end
end
