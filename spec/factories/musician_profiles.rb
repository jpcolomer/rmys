# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :musician_profile do
    user
    name {Faker::Name.name}
    edito {Faker::Lorem.paragraph(3)}

    factory :valid_musician do
      validation true
    end

    factory :invalid_musician do
      validation false
    end

  end
end
