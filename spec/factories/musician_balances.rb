# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :musician_balance do
    musician_profile
    credit { rand(1..100) }
  end
end
