FactoryGirl.define do 
  factory :genre do
    name { Genre::GENRES.sample }
    factory :invalid_genre do
      name 'sth'
    end
  end 
end