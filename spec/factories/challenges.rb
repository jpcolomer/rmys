FactoryGirl.define do 
  factory :challenge do
    user
    song
    before(:create) do |challenge|
      challenge.genres = build_list(:genre, 1)
    end
  end 
end