RSpec.configure do |config|
  config.before(:each, js: true) do
    Capybara.current_session.driver.browser.manage.window.resize_to(1800, 1024)
  end
end