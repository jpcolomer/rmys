require 'faker'
module MusicianMacros
  def fill_concert_form
    fill_in "Name", with: "Concert1"
    fill_in "Description", with: Faker::Lorem.paragraph(3)
    choose 'concert_duration_10'
    date = 1.month.from_now
    select date.year, from: "concert_date_1i"
    select date.strftime("%B"), from: "concert_date_2i"
    select date.day, from: "concert_date_3i"
    select date.strftime("%H"), from: "concert_date_4i"
    select date.strftime("%M"), from: "concert_date_5i"
    click_button 'Create'
  end

  def pick_challenge(challenge)
      within "#challenge_#{challenge.id}" do
      expect(page).to have_content challenge.genres_list
      expect(page).to have_content challenge.artist
      expect(page).to have_content challenge.song
      click_link 'PLAY'
    end
  end
end