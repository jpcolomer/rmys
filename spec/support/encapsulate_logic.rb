module EncapsulateLogicMacros

  def opens_payment_page
    click_link 'Buy Credits'
  end

  def buy_credits(credit)
    fill_in "Purchase Credits", with: credit
    click_button 'Buy'    
  end

  def goes_to_user_profile(user, name)
    click_link name
    click_link 'Fan'    
    expect(current_path).to eq user_path(user)
  end


  def open_buy_concert_ticket(concert)
    within "#concert_#{concert.id}" do
      click_link 'BUY TICKET'
    end   
  end

  def buy_ticket
    click_button 'Buy'
  end

  def user_sees_message (message)
    expect(page).to have_content message
  end

  def opens_concert_tip_page(concert)
    within "#concert_#{concert.id}" do
      click_link 'TIP this Musician'
    end    
    expect(current_path).to eq root_path
  end

  def opens_musician_tip_page(musician)
      visit musician_profile_path(musician)
      click_link 'TIP this Musician'    
  end

  def gives_tip(tip)
    within '#give-tip-modal' do
      fill_in "Tip", with: tip
      click_button 'Buy'
    end
  end

  def navigate_to_next_concert
    find('.next-wrapper').click
  end

  def doesnt_change_user_balance(user)
    expect {
      click_button 'Buy'
      user.reload
    }.to_not change{ user.balance_credit }
  end

  def give_donation(donation)
    fill_in "ticket_donation", with: donation
  end

  def musician_balance_increases_by(musician, credits)
    expect {
      musician.balance.reload
    }.to change {musician.balance_credit}.by(credits)    
  end

  def user_doesnt_see(element)
    expect(page).to_not have_content element
  end


end