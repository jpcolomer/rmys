module AuthenticationMacros

  def log_in(user1)
    within '#log-in-modal' do
      fill_in "Email", with: user1.email
      fill_in "Password", with: user1.password
      click_button 'Log in'
    end
  end

  def log_in_as(user)
    visit root_path
    click_link 'Profile'
    click_link 'Sign in'
    log_in(user)
  end

  def sign_in(user)
    visit new_user_session_path
    within '#new_user' do
      fill_in "Email", with: user.email
      fill_in "Password", with: user.password
      click_button 'Sign in'
    end
  end

end