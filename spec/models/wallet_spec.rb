require 'spec_helper'

describe Wallet do

  context 'with user balance' do
    let(:balance) {build_stubbed(:balance)}
    subject {Wallet.new(balance)}

    it {should be_true}
    its(:user_balance) {should eq balance}

  end

  context 'with user and musician balances' do
    let(:balance) {build_stubbed(:balance)}
    let(:musician_balance) {build_stubbed(:musician_balance)}
    subject {Wallet.new(balance, musician_balance)}

    it {should be_true}
    its(:user_balance) {should eq balance}
    its(:musician_balance) {should eq musician_balance}
  end

  describe '#credit' do
    context 'with user balance' do
      let(:balance) {build_stubbed(:balance, credit: 5)}
      subject {Wallet.new(balance)}

      its(:credit) {should eq 5}

      it 'returns 10 for user balance 10' do
        balance = build_stubbed(:balance, credit: 10)
        wallet = Wallet.new(balance)
        expect(wallet.credit).to eq 10
      end

    end

    context 'with user and musician balance' do
      let(:balance) {build_stubbed(:balance, credit: 5)}
      let(:musician_balance) {build_stubbed(:musician_balance, credit: 2)}
      subject {Wallet.new(balance, musician_balance)}

      its(:credit) {should eq 7}

      it 'returns 11 for user balance 3 and musician balance 8' do
        balance = build_stubbed(:balance, credit: 3)
        musician_balance = build_stubbed(:musician_balance, credit: 8)
        wallet = Wallet.new(balance, musician_balance)
        expect(wallet.credit).to eq 11
      end

    end  
  end

  describe '#has_credit?' do
    let(:balance) { build_stubbed(:balance, credit: 0) }

    context 'with user balance' do

      it 'returns false if credit is 0' do
          wallet = Wallet.new(balance)
          expect(wallet.has_credit?).to be_false
      end

      it 'returns true if user credit > 0' do
          balance = build_stubbed(:balance, credit: 1)
          wallet = Wallet.new(balance)
          expect(wallet.has_credit?).to be_true
      end

    end

    context 'with user and musician balance' do
      let(:musician_balance) { build_stubbed(:musician_balance, credit: 0) }

      it 'returns false if credit is 0' do
          wallet = Wallet.new(balance, musician_balance)
          expect(wallet.has_credit?).to be_false
      end

      it 'returns true if user credit > 0 and musician is 0' do
          balance = build_stubbed(:balance, credit: 1)
          wallet = Wallet.new(balance, musician_balance)
          expect(wallet.has_credit?).to be_true
      end

      it 'returns true if user credit is 0 and musician > 0' do
          musician_balance = build_stubbed(:musician_balance, credit: 1)
          wallet = Wallet.new(balance, musician_balance)
          expect(wallet.has_credit?).to be_true
      end

    end
  end

  describe '#has_enough_credit?' do
    let(:balance) { build_stubbed(:balance, credit: 5) }
    let(:musician_balance) { build_stubbed(:musician_balance, credit: 5) }

    it 'returns true for credit > arg' do
      wallet = Wallet.new(balance, musician_balance)
      expect(wallet.has_enough_credit?(5)).to be_true
    end

    it 'returns false for credit < arg' do
      wallet = Wallet.new(balance, musician_balance)
      expect(wallet.has_enough_credit?(15)).to be_false
    end
  end


  describe '#reduce_credit' do
    context "given enough credit" do
      let(:balance) { create(:balance, credit: 5) }

      context "with user balance" do
        
        it 'reduces credit from 5 to 0' do
          wallet = Wallet.new(balance)
          expect{wallet.reduce_credit(5)}.to change{ wallet.credit }.from(5).to(0)
        end


        it 'reduces credit from 25 to 5' do
          balance = create(:balance, credit: 25)
          wallet = Wallet.new(balance)
          expect{wallet.reduce_credit(20)}.to change{ wallet.credit }.from(25).to(5)
        end
      end

      context "with user and musician balance" do
        it 'reduces credit from 10 to 0' do
          musician_balance = create(:musician_balance, credit: 5)
          wallet = Wallet.new(balance, musician_balance)
          expect{wallet.reduce_credit(10)}.to change{ wallet.credit }.from(10).to(0)
        end

        context 'given amount > user balance' do
          let(:musician_balance) { create(:musician_balance, credit: 25) }

          it 'reduces user balance to 0' do
            wallet = Wallet.new(balance, musician_balance)
            expect{wallet.reduce_credit(25)}.to change{ balance.credit }.from(5).to(0)
          end

          it 'reduces the rest from musician balance' do
            wallet = Wallet.new(balance, musician_balance)
            expect{wallet.reduce_credit(25)}.to change{ musician_balance.credit }.from(25).to(5)
          end
        end
      end
    end

    context 'without enough credit' do
      let(:balance) { build_stubbed(:balance, credit: 5) }

      context "with user balance" do

        subject(:wallet) { Wallet.new(balance) }

        it 'returns nil' do
          expect(wallet.reduce_credit(25)).to be_nil
        end

        it "doesn't change user balance" do
          expect{ wallet.reduce_credit(25) }.to_not change{ balance.credit }
        end
      end

      context "with user and musician balance" do

        let(:musician_balance) { build_stubbed(:musician_balance, credit: 5) }
        subject(:wallet) { Wallet.new(balance, musician_balance) }

        it 'returns nil' do
          expect(wallet.reduce_credit(25)).to be_nil
        end

        it "doesn't change user balance" do
          expect{ wallet.reduce_credit(25) }.to_not change{ balance.credit }
        end

        it "doesn't change musician balance" do
          expect{ wallet.reduce_credit(25) }.to_not change{ musician_balance.credit }
        end
      end

    end
  end

end
