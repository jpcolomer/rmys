require 'spec_helper'

describe Artist do
  it {should have_many(:songs)}
  it {should have_many(:challenges).through(:songs)}

  context "given artist" do
    subject {create(:artist)}

    it do
      subject.stub(:capitalize_name).and_return(true)
      subject.should validate_presence_of(:name)
    end

    it {should validate_uniqueness_of(:name)}   
  end

  context "given valid params" do
    
    it 'is valid' do
      expect(build(:artist)).to be_valid
    end

    it 'gets save to the DB' do
      expect{ create(:artist) }.to change{ Artist.count }.by(1)
    end

  end


end
