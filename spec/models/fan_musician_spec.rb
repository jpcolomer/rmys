require 'spec_helper'

describe FanMusician do
  it {should belong_to(:musician)}
  it {should belong_to(:fan)}

  it {should validate_presence_of(:fan_id)} 
  it {should validate_presence_of(:musician_id)}  
  it {should validate_uniqueness_of(:fan_id).scoped_to(:musician_id)}
end
