require 'spec_helper'

describe Song do
  it {should belong_to(:artist)}
  it {should have_many(:challenges)}


  context "given song" do
    subject {create(:song)}

    it do
      subject.stub(:capitalize_name).and_return(true)
      subject.should validate_presence_of(:name)
    end

    it {should validate_presence_of(:artist)}
    it {should validate_uniqueness_of(:name).scoped_to(:artist_id)}   
    it {should accept_nested_attributes_for(:artist)}
  end

  context "given valid params" do
    
    it 'is valid' do
      expect(build(:song)).to be_valid
    end

    it 'gets save to the DB' do
      expect{ create(:song) }.to change{ Song.count }.by(1)
    end

  end

end