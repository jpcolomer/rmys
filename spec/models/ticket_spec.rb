require 'spec_helper'

describe Ticket do
  it {should belong_to(:user)}
  it {should belong_to(:concert)}
  it {should_not allow_value(-1).for(:donation)}
  it {should_not allow_value(0).for(:donation)}
  it {should_not allow_value(1.1).for(:donation)}

  context "given valid params" do
    
    it 'is valid' do
      expect(build(:ticket_with_wealthy_user)).to be_valid
    end

    it 'gets save to the DB' do
      expect{ create(:ticket_with_wealthy_user) }.to change{ Ticket.count }.by(1)
    end

  end


  it 'fails validation without concert' do
    expect(build(:ticket, concert: nil)).to have(1).errors_on(:concert)
  end

  it 'passes validation with concert' do
    expect(build(:ticket, concert: build(:concert))).to have(:no).errors_on(:concert)
  end

  it 'fails validation without user' do
    expect(build(:ticket, user: nil)).to have(1).errors_on(:user)
  end

  it 'passes validation with user' do
    expect(build(:ticket, user: create(:user))).to have(:no).errors_on(:user)
  end

  it {should validate_numericality_of(:donation).only_integer}

  it 'set amount to 20 with donation 10' do
    ticket = create(:ticket_with_wealthy_user, donation: 10)
    expect(ticket.amount).to eq 20
  end


  it 'set amount to 25 with donation 15' do
    ticket = create(:ticket_with_wealthy_user, donation: 15)
    expect(ticket.amount).to eq 25
  end

  context "given a user" do
    let(:user) { build_stubbed(:user) }
    let(:donation) { 10 }
    let(:amount) { 10 }

    it 'fails validation when has no credit' do
      user.stub(:has_enough_credit?).with(amount + donation).and_return(false)
      ticket = build(:ticket, donation: donation, user: user)
      expect(ticket).to have_at_least(1).errors_on(:credit)
    end


    it 'passes validation when has credit' do
      user.stub(:has_enough_credit?).with(amount + donation).and_return(true)
      expect(build(:ticket, donation: donation, user: user)).to have(:no).errors_on(:credit)
    end 


  end


  it "doesn't allow duplication per user and concert" do
    user = create(:wealthy_user)
    concert = create(:concert)
    create(:ticket, user: user, concert: concert)
    expect(build(:ticket, user: user, concert: concert)).to have(1).errors_on(:concert_id)
  end

  describe '#charge_user' do
    it 'reduces user credit by 12 with donation 2' do
      user = create(:wealthy_user)
      expect{ create(:ticket, donation: 2, user: user) }.to change { user.balance.credit }.by(-12)
    end

    it 'reduces user credit by 17 with donation 7' do
      user = create(:wealthy_user)
      expect{ create(:ticket, donation: 7, user: user) }.to change { user.balance.credit }.by(-17)
    end

  end

  describe '#increase_musician_credit' do 
    it 'increases musician credit by 12 with donation 2' do
      concert = create(:concert)
      expect{create(:ticket_with_wealthy_user, donation: 2, concert: concert)}.to change { concert.musician_balance.credit }.by(12)
    end

    it 'increases musician credit by 15 with donation 5' do
      concert = create(:concert)
      expect{create(:ticket_with_wealthy_user, donation: 5, concert: concert)}.to change { concert.musician_balance.credit }.by(15)
    end
  end




  it 'passes validation with amount' do
    expect(build(:ticket, amount: 1)).to have(:no).errors_on(:amount)
  end

end
