require 'spec_helper'

describe Challenge do
  it {should belong_to(:user)}
  it {should belong_to(:song)}

  it {should have_many(:concerts)}
  it {should have_many(:genres)}

  it {should validate_presence_of(:user)}
  it {should validate_presence_of(:song)}
  it {should validate_presence_of(:genres)}

  it { should accept_nested_attributes_for :song }
  it { should accept_nested_attributes_for :genres }
  it { should validate_presence_of :genres }

  describe '#artist' do
    it {should respond_to(:artist)}
    it 'returns song artist' do
      challenge = build_stubbed(:challenge)
      expect(challenge.artist).to eq challenge.song.artist
    end
  end

  describe '#artist_name' do
    it 'returns artist name' do
      challenge = build_stubbed(:challenge)
      expect(challenge.artist_name).to eq challenge.song.artist.name
    end
  end

  describe '#song_name' do
    it 'returns song name' do
      challenge = build_stubbed(:challenge)
      expect(challenge.song_name).to eq challenge.song.name
    end
  end

  describe '.create_with_attributes' do
    context "given new artist" do
      context "and given song" do
        let(:user) {create(:user)}
        let(:challenge_attributes) { attributes_for(:challenge, user_id: user.id,
                                    genres_attributes: {0 => attributes_for(:genre)}, 
                                    song_attributes: attributes_for(:song,
                                  artist_attributes: attributes_for(:artist) ))}

        it "changes artist DB count by 1" do
          expect { Challenge.create_with_attributes(challenge_attributes) }.to change{ Artist.count }.by(1)
        end

        it "changes song DB count by 1" do
          expect { Challenge.create_with_attributes(challenge_attributes) }.to change{ Song.count }.by(1)
        end

        it "changes challenge DB count by 1" do
          expect { Challenge.create_with_attributes(challenge_attributes) }.to change{ Challenge.count }.by(1)
        end

        it "returns result true" do
          Challenge.create_with_attributes(challenge_attributes)[0].should == true
        end
      end

    end

    context "given an artist in DB" do
      let(:user) {create(:user)}
      before(:each) do
        @artist = create(:artist)
      end

      context "and given new song" do
        let(:challenge_attributes) { attributes_for(:challenge, user_id: user.id,
                                    genres_attributes: {0 => attributes_for(:genre)}, 
                                    song_attributes: attributes_for(:song,
                                  artist_attributes: attributes_for(:artist, name: @artist.name) ))}

        it "doesn't change artist DB count by 1" do
          expect { Challenge.create_with_attributes(challenge_attributes) }.to_not change{ Artist.count }
        end

        it "changes song DB count by 1" do
          expect { Challenge.create_with_attributes(challenge_attributes) }.to change{ Song.count }.by(1)
        end

        it "changes challenge DB count by 1" do
          expect { Challenge.create_with_attributes(challenge_attributes) }.to change{ Challenge.count }.by(1)
        end
        it "returns result true" do
          Challenge.create_with_attributes(challenge_attributes)[0].should == true
        end
      end

      context "and given song in DB" do
        before(:each) do
          @song = create(:song, artist: @artist)
        end

        let(:challenge_attributes) { attributes_for(:challenge, user_id: user.id,
                                    genres_attributes: {0 => attributes_for(:genre)}, 
                                    song_attributes: attributes_for(:song, name: @song.name,
                                  artist_attributes: attributes_for(:artist, name: @artist.name) ))}

        it "doesn't change artist DB count by 1" do
          expect { Challenge.create_with_attributes(challenge_attributes) }.to_not change{ Artist.count }
        end

        it "changes song DB count by 1" do
          expect { Challenge.create_with_attributes(challenge_attributes) }.to_not change{ Song.count }
        end

        it "changes challenge DB count by 1" do
          expect { Challenge.create_with_attributes(challenge_attributes) }.to change{ Challenge.count }.by(1)
        end

        it "returns result true" do
          Challenge.create_with_attributes(challenge_attributes)[0].should == true
        end
      end      
    end

    context 'given invalid artist' do
      let(:user) {create(:user)}
      let(:challenge_attributes) { attributes_for(:challenge, user_id: user.id,
                                  genres_attributes: {0 => attributes_for(:genre)}, 
                                  song_attributes: attributes_for(:song,
                                  artist_attributes: attributes_for(:artist, name:'') ))}

      it "returns result false" do
        Challenge.create_with_attributes(challenge_attributes)[0].should == false
      end

      it "doesn't change the DB" do
        expect { Challenge.create_with_attributes(challenge_attributes) }.to_not change{ Artist.count }
        expect { Challenge.create_with_attributes(challenge_attributes) }.to_not change{ Song.count }
        expect { Challenge.create_with_attributes(challenge_attributes) }.to_not change{ Challenge.count }
      end
    end

    context 'given invalid song' do
      let(:user) {create(:user)}
      let(:challenge_attributes) { attributes_for(:challenge, user_id: user.id,
                                  genres_attributes: {0 => attributes_for(:genre)}, 
                                  song_attributes: attributes_for(:song, name: '',
                                  artist_attributes: attributes_for(:artist)))}

      it "returns result false" do
        Challenge.create_with_attributes(challenge_attributes)[0].should == false
      end

      it 'records the Artist to the DB' do
        expect { Challenge.create_with_attributes(challenge_attributes) }.to change{ Artist.count }.by(1)
      end

      it "doesn't record song to the DB" do
        expect { Challenge.create_with_attributes(challenge_attributes) }.to_not change{ Song.count }
      end

      it "doesn't record song to the DB" do
        expect { Challenge.create_with_attributes(challenge_attributes) }.to_not change{ Challenge.count }
      end
    end
  end





end
