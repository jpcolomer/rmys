require 'spec_helper'

describe PendingMusicianValidation do
  ### MUST REVIEW THIS MODEL, MIGHT BE BETTER TO JUST USE THE BOOLEAN FIELD ON MUSICIAN PROFILE

  it {should belong_to(:musician_profile)}

  describe "#next" do
    let(:pm1) { create(:musician_profile).pending_musician_validation }
    let(:pm2) { create(:musician_profile).pending_musician_validation }
    
    before :each do 
      pm1; pm2
    end

    it "returns next pending musician validation" do
      expect(pm1.next).to eq pm2
    end

    it "returns nil when no more pending validations" do
      expect(pm2.next).to be_nil
    end
  end

  describe "#previous" do
    let(:pm1) { create(:musician_profile).pending_musician_validation }
    let(:pm2) { create(:musician_profile).pending_musician_validation }

    before :each do 
      pm1; pm2
    end

    it "returns previous pending musician validation" do
      expect(pm2.previous).to eq pm1
    end

    it "returns nil when no more pending validations" do
      expect(pm1.previous).to be_nil
    end
  end


end