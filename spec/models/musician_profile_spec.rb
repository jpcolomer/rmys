require 'spec_helper'

describe MusicianProfile do
  it {should allow_mass_assignment_of(:facebook_url)}
  it {should belong_to(:user)}
  it {should have_one(:pending_musician_validation)}
  it {should have_one(:balance)}
  it {should have_many(:concerts)}
  it {should have_many(:fan_musicians)}
  it {should have_many(:fans)}
  it {should have_many(:tips)}
  it {should validate_presence_of(:user_id)}
  it {should validate_presence_of(:name)}
  it {should validate_uniqueness_of(:user_id)}
  it {should_not allow_value("https://test.com").for(:youtube_url) }
  it {should_not allow_value("httpsasgf://test.com").for(:youtube_url) }
  it {should allow_value("http://www.youtube.com/watch?v=GUULNJEdKU8").for(:youtube_url) }
  it {should_not allow_value("https://test.com").for(:facebook_url) }
  it {should_not allow_value("httpsasgf://test.com").for(:facebook_url) }
  it {should allow_value("http://www.facebook.com/pages/Rockmystage/341656695946293?fref=ts").for(:facebook_url) }
  it {should_not allow_value("httpsasgf://test.com").for(:other_url) }
  it {should allow_value("http://www.youtube.com/watch?v=GUULNJEdKU8").for(:other_url) }
  it {should respond_to(:balance_credit)}


  context "given valid params" do
    
    it 'is valid' do
      expect(build(:musician_profile)).to be_valid
    end

    it 'gets save to the DB' do
      expect{ create(:musician_profile) }.to change{ MusicianProfile.count }.by(1)
    end

  end


  it 'builds musician balance on creation' do
    musician = create(:musician_profile)
    expect(musician.balance).to be_kind_of(MusicianBalance)
  end

  it 'builds pending validation on creation' do
    ### REFACTOR PENDING VALIDATION TABLE TO JUST A BOOLEAN FIELD ON MUSICIAN PROFILE
    musician = create(:musician_profile)
    expect(musician.pending_musician_validation).to be_kind_of(PendingMusicianValidation)
  end


  describe '#can_play_concert?' do
    subject { create(:musician_profile) }
    it 'returns true when has no concerts' do
      expect(subject.can_play_concert?).to be_true
    end

    it 'returns false when has concert' do
      create(:concert, musician_profile: subject)
      expect(subject.can_play_concert?).to be_false
    end
  end

  describe '#has_fan?' do
    subject { create(:musician_profile) }
    let(:user) { create(:user) }
    it "returns true when user is fan" do
      subject.fans << user
      expect(subject.has_fan?(user)).to be_true
    end    
    it "returns false when user isn't fan" do
      expect(subject.has_fan?(user)).to be_false
    end    
  end

  describe '#increase_credit' do
    subject { create(:musician_profile) }

    it 'increases balance by 2' do
      expect{subject.increase_credit(2)}.to change{ subject.balance_credit }.by(2)
    end

    it 'increases balance by 5' do
      expect{subject.increase_credit(5)}.to change{ subject.balance_credit }.by(5)
    end

  end

  describe '#domain_for_other_url' do
    it 'returns facebook.com' do
      musician = build(:musician_profile, other_url: 'http://www.facebook.com/blabla')
      expect(musician.domain_for_other_url).to eq 'facebook.com'
    end

    it 'returns facebook.com' do
      musician = build(:musician_profile, other_url: 'http://facebook.com/blabla')
      expect(musician.domain_for_other_url).to eq 'facebook.com'
    end
  end


  describe '#valid_concerts' do
    let(:musician) { create(:valid_musician) }
    it 'returns non performed concerts with date > now' do
      concert = create(:concert, musician_profile: musician)
      create(:performed_concert, musician_profile: musician)
      expect(musician.valid_concerts).to match_array [concert]
    end

    it "doesnt return performed concerts" do
      performed_concert = create(:performed_concert, musician_profile: musician)
      expect(musician.valid_concerts).to_not include performed_concert
    end
  end

end
