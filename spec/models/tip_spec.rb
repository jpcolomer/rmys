require 'spec_helper'

describe Tip do

  it {should belong_to(:user)}
  it {should belong_to(:musician_profile)}
  it {should_not allow_value(-1).for(:amount)}
  it {should_not allow_value(0).for(:amount)}
  it {should_not allow_value(1.1).for(:amount)}


  it 'fails validation without amount' do
    expect(build(:tip, amount: nil)).to have_at_least(1).errors_on(:amount)
  end

  it 'passes validation with amount' do
    expect(build(:tip, amount: 1)).to have(:no).errors_on(:amount)
  end


  context "given valid params" do
    
    it 'is valid' do
      expect(build(:tip_with_user_credit)).to be_valid
    end

    it 'gets save to the DB' do
      expect{ create(:tip_with_user_credit) }.to change{ Tip.count }.by(1)
    end

  end



  context "given user" do
    let(:user) { build_stubbed(:user) }
    let(:amount) { 10 }

    it 'fails validation when user has no credit' do
      user.stub(:has_enough_credit?).with(amount).and_return(false)
      expect(build(:tip, amount: amount, user: user)).to have_at_least(1).errors_on(:credit)
    end

    it 'passes validation when user has credit' do
      user.stub(:has_enough_credit?).with(amount).and_return(true)
      expect(build(:tip, amount: amount, user: user)).to have(:no).errors_on(:credit)
    end  


  end


  it 'fails validation without musician_profile_id' do
    expect(build(:tip, musician_profile_id: nil)).to have(1).errors_on(:musician_profile_id)
  end

  it 'passes validation with musician_profile_id' do
    expect(build(:tip)).to have(:no).errors_on(:musician_profile_id)
  end

  describe '#charge_user' do
    let(:user) { user = create(:wealthy_user) }

    it 'reduces user credit by 2' do
      expect{ create(:tip, amount: 2, user: user) }.to change { user.balance.credit }.by(-2)
    end

    it 'reduces user credit by 5' do
      expect{ create(:tip, amount: 5, user: user) }.to change { user.balance.credit }.by(-5)
    end
  end

  describe '#increase_musician_credit' do 
    let(:musician) { create(:musician_profile) }

    it 'increases musician credit by 2' do
      expect{create(:tip_with_user_credit, amount: 2, musician_profile: musician)}.to change { musician.balance.credit }.by(2)
    end

    it 'increases musician credit by 5' do
      expect{create(:tip_with_user_credit, amount: 5, musician_profile: musician)}.to change { musician.balance.credit }.by(5)
    end
  end

end