require 'spec_helper'

describe Genre do
  it {should belong_to(:challenge)}
  it {should validate_presence_of(:name)}
  it { should ensure_inclusion_of(:name).in_array(Genre::GENRES) }

  context "given valid params" do
    
    it 'is valid' do
      expect(build(:genre)).to be_valid
    end

    it 'gets save to the DB' do
      expect{ create(:genre) }.to change{ Genre.count }.by(1)
    end

  end

  context 'given invalid params' do
    it 'is invalid' do
      expect(build(:invalid_genre)).to_not be_valid
    end
  end
end
