require 'spec_helper'
require 'faker'

describe Concert do
  it {should belong_to(:challenge)}
  it {should belong_to(:musician_profile)}

  it {should have_many(:tickets)}
  it {should have_many(:users)}

  context "given valid params" do
    
    it 'is valid' do
      expect(build(:concert)).to be_valid
    end

    it 'gets save to the DB' do
      expect{ create(:concert) }.to change{ Concert.count }.by(1)
    end

    it 'has end_date on DB' do
      concert = create(:concert)
      expect(concert.end_date).to eq (concert.date + concert.duration.minutes)
    end

  end

  context 'given a concert' do
    subject {build_stubbed(:concert)}

    it {should validate_presence_of(:date)}
    it {should validate_presence_of(:name)}
    it {should validate_presence_of(:challenge)}
    it {should validate_presence_of(:duration)}

    it 'should not allow description length > 300' do
      should_not allow_value(Faker::Lorem.characters(301)).for(:description)
    end

    it 'should allow description length == 300' do
      should allow_value(Faker::Lorem.characters(300)).for(:description)
    end

    it 'is invalid without musician_profile' do
      concert = build(:concert, musician_profile: nil)
      concert.stub(:musician_has_more_than_one_non_performed_concert).and_return(true)
      expect(concert).to have(1).errors_on(:musician_profile)
    end

    it 'is valid with musician_profile' do
      concert = build(:concert)
      concert.stub(:musician_has_more_than_one_non_performed_concert).and_return(true)
      expect(concert).to have(:no).errors_on(:musician_profile)
    end

    it {should validate_numericality_of(:duration)}

    it "can't have more than 1 concert not performed" do
      musician = build_stubbed(:musician_profile)
      create(:concert, musician_profile: musician)
      concert = build(:concert, musician_profile: musician)
      expect(concert).to have(1).errors_on(:musician_profile_id)
    end

    it "can have 1 concert not performed and multiple performed" do
      musician = build_stubbed(:musician_profile)
      create_list(:performed_concert,2, musician_profile: musician)
      concert = build(:concert, musician_profile: musician)
      expect(concert).to have(:no).errors_on(:musician_profile_id)
    end

    it 'is invalid with date < Now' do
      concert = build(:concert, date: 1.days.ago)
      expect(concert).to have(1).errors_on(:date)
    end
  end


  describe '.non_performed' do
    before(:each) do
      create(:performed_concert)
    end

    it 'returns all non performed concerts' do
      concert = create(:concert)
      expect(Concert.non_performed).to match_array [concert]
    end

    it "returns empty array when there aren't non performed concerts" do
      expect(Concert.non_performed).to be_empty
    end
  end

  describe '.recent' do
    it "returns ordered array" do
      concert1 = create(:concert)
      concert2 = create(:concert)
      expect(Concert.recent).to eq [concert2, concert1]
    end
  end

  describe '.greater_date' do
    let(:concert1) { create(:concert, date: 2.days.from_now) }
    let(:concert2) { create(:concert, date: 11.days.from_now) }

    it "returns concerts with date > 10 days from now" do
      expect(Concert.greater_date(10.days.from_now)).to match_array [concert2]
    end

    it "doesn't return concerts with date < 10 days from now" do
      expect(Concert.greater_date(10.days.from_now)).to_not include concert1
    end
  end

  describe '.less_date' do
    let(:concert1) { create(:concert, date: 2.days.from_now) }
    let(:concert2) { create(:concert, date: 11.days.from_now) }

    it "returns concerts with date < 10 days from now" do
      expect(Concert.less_date(10.days.from_now)).to match_array [concert1]
    end

    it "doesn't return concerts with date < 10 days from now" do
      expect(Concert.less_date(10.days.from_now)).to_not include concert2
    end
  end


  describe '.closest' do
    it "returns ordered concerts by performance day" do
      concert1 = create(:concert, date: 1.days.from_now)
      concert2 = create(:concert, date: 2.days.from_now)
      expect(Concert.closest).to eq [concert1, concert2]
    end
  end


  describe '.closest_concert' do
    it "returns closest non performed concert" do
      concert = create(:concert, date: 2.days.from_now)
      create(:concert, date: 3.days.from_now)
      create(:performed_concert)
      expect(Concert.closest_concert).to eq concert
    end

    it "returns nil when there aren't non performed concert" do
      create(:performed_concert)
      expect(Concert.closest_concert).to eq nil
    end
  end

  describe '.farest_concert' do
    it "returns farest non performed concert" do
      concert = create(:concert, date: 5.days.from_now)
      create(:concert, date: 3.days.from_now)
      create(:performed_concert)
      expect(Concert.farest_concert).to eq concert
    end

    it "returns nil when there aren't non performed concert" do
      create(:performed_concert)
      expect(Concert.farest_concert).to eq nil
    end
  end

  describe '#next' do
    it "returns next non performed concert" do
      concert = build_stubbed(:concert, date: 2.days.from_now)
      next_concert = create(:concert, date: 3.days.from_now)
      expect(concert.next).to eq next_concert
    end

    context "given no more non performed concerts" do

      it "returns nil" do
        first_concert = create(:concert, date: 2.days.from_now)
        concert = build_stubbed(:concert, date: 3.days.from_now)
        expect(concert.next).to eq nil
      end
       
    end
  end



  describe '#previous' do
    it "returns previous non performed concert" do
      first_concert = create(:concert, date: 2.days.from_now)
      last_concert = build_stubbed(:concert, date: 3.days.from_now)
      expect(last_concert.previous).to eq first_concert
    end

    context "given no more non performed concerts" do

      it "returns nil" do
        first_concert = build_stubbed(:concert, date: 2.days.from_now)
        last_concert = create(:concert, date: 3.days.from_now)
        expect(first_concert.previous).to eq nil
      end
       
    end
  end

  describe '#countdown' do
    it 'returns days, hours and minutes' do
      concert = build_stubbed(:concert)
      diff = stub(days: 2, hours: 10, minutes: 20)
      DateTime.any_instance.stub(:difference_in_days_hours_mins).with(concert.date).and_return(diff)
      expect(concert.countdown).to eq  ('%.2d days, %.2d hours, %.2d minutes' % [2, 10, 20])
    end

    it 'calls difference_in_days_hours_mins' do
      concert = build_stubbed(:concert)
      diff = stub(days: 2, hours: 10, minutes: 20)
      DateTime.any_instance.should_receive(:difference_in_days_hours_mins).with(concert.date).and_return(diff)
      concert.countdown
    end
  end

  describe 'concert_genres' do
    it "returns string of genres" do
      challenge = create(:challenge)
      concert = create(:concert, challenge: challenge)
      expect(concert.genres).to eq challenge.genres.join(', ')
    end
  end

  describe '#musician_balance' do
    it 'returns the musician balance' do
      concert = build_stubbed(:concert)
      expect(concert.musician_balance).to eq concert.musician_profile.balance
    end
  end
end
