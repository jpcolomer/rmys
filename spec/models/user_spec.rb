require 'spec_helper'

describe User do

  it {should have_one(:musician_profile)}
  it {should have_one(:balance)}
  it {should have_many(:challenges)}
  it {should have_many(:fan_musicians)}
  it {should have_many(:musicians).through(:fan_musicians)}
  it {should have_many(:tickets)}
  it {should have_many(:concerts)}
  it {should have_many(:tips)}
  it {should have_many(:payments)}

  it {should validate_presence_of(:email)}
  it {should validate_presence_of(:name)}
  it {should validate_uniqueness_of(:email)}

  it {should respond_to(:balance_credit)}


  context "given valid params" do
    
    it 'is valid' do
      expect(build(:user)).to be_valid
    end

    it 'gets save to the DB' do
      expect{ create(:user) }.to change{ User.count }.by(1)
    end

  end


  it 'builds balance before creation' do
    expect(create(:user).balance).to be_kind_of(Balance)
  end

  describe '#is_musician?' do
    context 'with musician profile' do
      it 'returns true for valid musician' do
        musician = create(:valid_musician)
        expect(musician.user.is_musician?).to be_true
      end
      it 'returns false for not valid musician' do
        musician = create(:musician_profile)
        expect(musician.user.is_musician?).to be_false
      end
    end
    context "without musician profile" do
      it 'returns false' do
        expect(create(:user).is_musician?).to be_false
      end      
    end
  end

  describe '#increase_credit' do
    let(:user) { create(:user) }
    it 'increases user credit by 2' do
      expect{user.increase_credit(2)}.to change{ user.balance_credit }.by(2)
    end
    it 'increases user credit by 5' do
      expect{user.increase_credit(5)}.to change{ user.balance_credit }.by(5)
    end

    it 'increases user credit by 10 without argument' do
      expect{user.increase_credit}.to change{ user.balance_credit }.by(10)
    end
  end

  describe '#has_ticket?' do
    context 'given a concert' do
      it 'returns true when has ticket' do
        ticket = create(:ticket_with_wealthy_user)
        expect(ticket.user.has_ticket?(ticket.concert)).to be_true
      end

      it 'returns false when doesn\'t has ticket' do
        user = create(:wealthy_user)
        concert = create(:concert)
        expect(user.has_ticket?(concert)).to be_false
      end

      it "returns true when concert is from himself" do
        user = create(:user)
        musician = create(:valid_musician, user: user)
        concert = create(:concert, musician_profile: musician)
        expect(user.has_ticket?(concert)).to be_true
      end
    end
  end

  describe '#has_credit?' do
    it 'returns true when user has credit' do
      user = create(:wealthy_user)
      expect(user.has_credit?).to be_true
    end

    it 'returns false when user has no credit' do
      user = create(:user)
      expect(user.has_credit?).to be_false
    end
  end

  describe '#has_enough_credit?' do
    it 'returns true when user has enough credit' do
      user = create(:wealthy_user)
      expect(user.has_enough_credit?(10)).to be_true
    end

    it 'returns false when user doesn\'t have enough credit' do
      user = create(:user)
      expect(user.has_enough_credit?(10)).to be_false
    end
  end

  describe '#reduce_credit' do
    let(:user) { create(:wealthy_user) }
    it 'reduces user balance by 2' do
      expect{ user.reduce_credit(2) }.to change{ user.balance_credit }.by(-2)
    end

    it 'reduces user balance by 5' do
      expect{ user.reduce_credit(5) }.to change{ user.balance_credit }.by(-5)
    end

    it 'reduces user balance by 10 without argument' do
      expect{ user.reduce_credit }.to change{ user.balance_credit }.by(-10)
    end
  end


  describe '#increase_credit' do
    let(:user) { create(:user) }
    it 'increases user balance by 2' do
      expect{ user.increase_credit(2) }.to change{ user.balance_credit }.by(2)
    end

    it 'increases user balance by 5' do
      expect{ user.increase_credit(5) }.to change{ user.balance_credit }.by(5)
    end

    it 'increases user balance by 10 without argument' do
      expect{ user.increase_credit }.to change{ user.balance_credit }.by(10)
    end
  end

end