require 'spec_helper'

describe Payment do
  it {should belong_to(:user)}
  it {should have_one(:balance).through(:user)}

  it {should validate_presence_of(:user)}
  it {should validate_presence_of(:amount)}
  it {should validate_numericality_of(:amount).only_integer}

  context "given valid params" do
    
    it 'is valid' do
      expect(build(:payment)).to be_valid
    end

    it 'gets save to the DB' do
      expect{ create(:payment) }.to change{ Payment.count }.by(1)
    end

  end


  it 'increases user credit by 10 after create' do

    user = create(:user)
    payment = user.payments.new(amount: 10)
    expect{ 
      payment.save
      user.balance.reload
    }.to change{ user.balance_credit }.by(10)
  end
end
