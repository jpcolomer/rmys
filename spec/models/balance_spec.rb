require 'spec_helper'

describe Balance do
  it {should belong_to(:user)}
  it {should validate_presence_of(:user_id)}
  it {should validate_presence_of(:credit)}

  context "given valid params" do
    
    it 'is valid' do
      expect(build(:balance)).to be_valid
    end

    it 'gets save to the DB' do
      expect{ Balance.create(credit: 0, user_id: 1) }.to change{ Balance.count }.by(1)
    end

  end


  describe '#increase_credit' do
    subject { create(:balance, credit: 10)}

    it 'increases credit by 2' do
      expect{ subject.increase_credit(2) }.to change{ subject.credit }.by(2)
    end

    it 'increases credit by 5' do
      expect{ subject.increase_credit(5) }.to change{ subject.credit }.by(5)
    end
  end

  describe '#reduce_credit' do
    subject { create(:balance, credit: 10)}  
    it 'reduces credit by 2' do
      expect{ subject.reduce_credit(2) }.to change{ subject.credit }.by(-2)
    end

    it 'reduces credit by 5' do
      expect{ subject.reduce_credit(5) }.to change{ subject.credit }.by(-5)
    end

  end

end
