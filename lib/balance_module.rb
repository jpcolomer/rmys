module BalanceBehaviour
  extend ActiveSupport::Concern

  included do
    attr_accessible :credit
    validates_presence_of :credit
  end

  def increase_credit(amount)
    self.credit += amount.to_i
    save
  end

  def reduce_credit(amount)
    self.credit -=  amount.to_i
    save
  end

end