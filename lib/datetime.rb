class DateTime
  def difference_in_days(date)
    ((date - self)/1.day).to_i
  end
  def difference_in_hours(date)
    ((date - self)/1.hour).to_i
  end
  def difference_in_mins(date)
    ((date - self)/60).to_i
  end
  def difference_in_days_hours_mins(date)
    days = difference_in_days(date)
    hours = ((date - self - days*1.day).to_f/1.hour).to_i
    minutes = ((date - self - days*1.day - hours*1.hour).to_f/60).to_i
    Struct.new(:days, :hours, :minutes).new(days, hours, minutes)
  end
end