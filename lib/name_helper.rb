module NameHelpers
  extend ActiveSupport::Concern

  included do
    before_validation :capitalize_name
  end

  def self.included(klass)
    klass.extend SearchMethods
  end

  def capitalize_name
    self.name = self.name.capitalize
  end
  
  module SearchMethods
    def find_by_name_like(name)
      where("name ilike ?", "%#{name}%")
    end
  end

end