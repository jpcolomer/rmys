module Charge
  extend ActiveSupport::Concern

  included do
    validate :user_has_credit?
    after_save :charge_user, :increase_musician_credit
  end

  def charge_user
    price = respond_to?(:amount) ? (amount || 10) : 10
    user.reduce_credit(price)
  end

  def user_has_credit?
    credit = respond_to?(:amount) ? (amount || 10) : 10
    if user.present?
      response = user.has_enough_credit?(credit)
      errors.add(:credit, 'not enough credit') unless response
    else
      response = false
    end
    response
  end

  def increase_musician_credit
    price = respond_to?(:amount) ? (amount || 10) : 10
    musician_profile.increase_credit(price)
  end


end