class SongsController < ApplicationController
  def index
    @songs = Song.find_by_name_like params[:name]
    @songs = Song.all unless params[:name]
    @songs = @songs.map{|song| song.name}
 
    render json: @songs
  end

end
