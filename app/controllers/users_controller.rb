class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :get_user
  before_filter :same_user?, only: :update

  def show
    @is_same_user = current_user == @user
    @challenges = @user.challenges.includes(:concerts => :musician_profile).includes(:song => :artist)
    @concerts_challenged = @challenges.inject([]) do |result, challenge|
      result << challenge.concerts.last if challenge.concerts.any?
      result
    end
    @concerts_payed = @user.concerts.non_performed.closest.greater_date.limit(5).includes(challenge: {song: :artist})
    @musicians = @user.musicians
  end

  def update
    respond_to do |format|
    @user = User.find(params[:id])
      if @user.update_attributes params[:user]
        format.html { redirect_to user_path(@user), notice: 'Picture was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { redirect_to :back }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def get_user
    @user = User.find(params[:id])
  end

  def same_user?
    unless current_user == @user
      flash[:error] = "You can only visit your profile"
      redirect_to :back # halts request cycle
    end
  end
end