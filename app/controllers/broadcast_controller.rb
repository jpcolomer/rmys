class BroadcastController < ApplicationController
  before_filter :authenticate_user!

  def show
    concert = Concert.find_by_id(params[:concert_id])
    respond_to do |format|
      # if !current_user.has_ticket?(concert)
      #   flash[:error] = "You need to buy a ticket to watch this concert"
      #   format.html { redirect_to root_path }
      #   format.js { js_redirect_to root_path }
      # elsif concert.date > 10.minutes.from_now
      #   flash[:notice] = 'Concert did not start yet! Please click on Play 10 min before'
      #   format.html { redirect_to root_path, notice: 'Concert did not start yet! Please click on Play 10 min before' }
      #   format.js { js_redirect_to root_path }
      # else
        format.html
        format.js
      # end
    end
  end
end
