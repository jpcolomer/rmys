class ConcertsController < ApplicationController
  before_filter :authenticate_user!, except: :show
  before_filter :validate_musician!, except: :show
  def new
    @concert = Concert.new
    @concert.challenge_id = params[:challenge] 
  end

  def destroy

    @concert = Concert.find(params[:id])
    @concert.destroy

    respond_to do |format|
      format.html { redirect_to :back, notice: 'Concert canceled!' }
      format.json { head :no_content }
    end
  end

  def create
    @concert = current_user.musician_profile.concerts.new params[:concert]
    if @concert.save
      redirect_to musician_profile_path(current_user.musician_profile), notice: "Concert created" 
    else
      render :new
    end
  end

  def show
    @concert = Concert.includes(:musician_profile => [:fans]).find(params[:id])
    if user_signed_in?
      @ticket = Ticket.new(user_id: current_user.id, concert_id: @concert.id)
      @musician_profile = @concert.musician
      @user_musician_profile = current_user.musician_profile
      @tip = Tip.new(musician_profile_id: @musician_profile.id, user_id: current_user.id)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  private
  def validate_musician!
    musician_profile = current_user.musician_profile
    unless musician_profile.valid_profile?
      flash[:error] = 'Your musician profile is not valid or hasn\'t been validated yet'
      redirect_to musician_profile_path(musician_profile)
    end  
  end
end
