class ChallengesController < ApplicationController
  helper_method :sort_column, :sort_direction

  before_filter :authenticate_user!, except: [:new, :create]
  before_filter :has_musician_profile?, except: [:new, :create]

  layout "application_with_main", except: :index

  def new
    @challenge = user_signed_in? ? current_user.challenges.new : Challenge.new
    @challenge.build_song.build_artist
    @genres = Genre::GENRES
    @genres.size.times{@challenge.genres.build}
  end

  def index
    @challenges = Challenge.joined_relations.order("#{sort_column}s.name" + ' ' + sort_direction)
  end

  def show
  end

  def create
    result, @challenge = Challenge.create_with_attributes(params[:challenge])
    if result
      redirect_to :back, notice: 'Challenge sent.'
    else
      @challenge.build_song.build_artist
      @genres = Genre::GENRES
      @genres.size.times{@challenge.genres.build}
      flash[:error] = 'Challenge couldn\'t be added.'
      render action: 'new'
    end 

  end

  private
  def has_musician_profile?
    has_profile = current_user.musician_profile.present?
    unless has_profile
      flash[:error] = 'Need a Musician Profile to play a challenge'
      redirect_to new_musician_profile_path
    end
  end

  def sort_column
    %w[genre artist song].include?(params[:sort]) ?  params[:sort].to_sym : "genre"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end


end
