class ArtistsController < ApplicationController
  def index
    @artists = Artist.find_by_name_like params[:name]
    @artists = Artist.all unless params[:name]
    @artists = @artists.map{|artist| artist.name}
    respond_to do |format|
      format.html 
      format.json { render json: @artists.to_json(only: :name)}
    end
  end
end
