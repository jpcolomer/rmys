class SessionsController < Devise::SessionsController

  def create
    session[:link_to] = params.delete :link_to if params.key?(:link_to)
    super
  end

end