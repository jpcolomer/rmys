class ValidationsController < ApplicationController
  before_filter :authenticate_admin!

  def index
    if PendingMusicianValidation.any?
      redirect_to validation_path(PendingMusicianValidation.first)
    else
      redirect_to root_path, notice: "No pending validations"
    end
  end

  def show
    @validation = PendingMusicianValidation.find(params[:id])
    @musician_profile = @validation.musician_profile
  end

  def create
    ### NEED REFACTOR, CREATE MUSICIAN PROFILE METHOD TO VALIDATE
    @musician_profile = MusicianProfile.find(params[:musician_profile_id])
    if @musician_profile.update_attributes(params[:musician_profile])
      next_validation = @musician_profile.pending_musician_validation.next
      previous_validation = @musician_profile.pending_musician_validation.previous
      @musician_profile.pending_musician_validation.destroy
      if PendingMusicianValidation.any?
        if next_validation
          redirect_to validation_path(next_validation) 
        else
          redirect_to validation_path(previous_validation)
        end
      else
        redirect_to root_path, notice: 'No more pending validations'
      end
    else
      render action: 'show'
    end
  end


end
