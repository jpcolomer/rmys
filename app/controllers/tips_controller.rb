class TipsController < ApplicationController
  before_filter :authenticate_user!

  def new
    @musician_profile = get_profile
    @user = get_user
    @user_musician_profile = @user.musician_profile
    @url = session[:first_url] || request.env["HTTP_REFERER"] || musician_profile_path(@musician_profile)
    @tip = Tip.new(musician_profile_id: @musician_profile.id, user_id: @user.id)
  end

  def create
    @musician_profile = get_profile
    @user = get_user
    @url = params[:redirect]
    @tip = Tip.new(params[:tip])


    ### NEED TO MAKE THIS DRYier, (TIP AND TICKET ARE VERY SIMILAR) ###
    if @tip.save
      session[:first_url] = nil
      respond_to do |format|
        flash[:notice] = "Successfully created...""You tipped this musician with #{@tip.amount} credits"
        format.html { redirect_to @url }
        format.js { js_redirect_to @url }
      end
    else
      unless @user.has_enough_credit?(params[:tip][:amount])
        flash[:error] = 'You are trying to give more credits than you have'
        session[:first_url] = @url
        redirect_to new_payment_path
      else
        render action: 'new'
      end
    end
  end


  private
  def get_profile
    MusicianProfile.find(params[:musician_profile_id])
  end
  def get_user
    current_user
  end
end
