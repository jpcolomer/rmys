class HomeController < ApplicationController
  layout "application_with_main"
  def index
    session[:link_to] = nil
    @concert = Concert.includes(:musician_profile => [:fans]).closest_concert
    @challenge = user_signed_in? ? current_user.challenges.new : Challenge.new
    @challenge.build_song.build_artist
    @genres = Genre::GENRES
    @challenge.genres.build
    if user_signed_in? && @concert
      @ticket = Ticket.new(user_id: current_user.id, concert_id: @concert.id)
      @musician_profile = @concert.musician
      @user_musician_profile = current_user.musician_profile
      # @url = session[:first_url] || request.env["HTTP_REFERER"] || musician_profile_path(@musician_profile)
      @tip = Tip.new(musician_profile_id: @musician_profile.id, user_id: current_user.id)
    end
  end
end
