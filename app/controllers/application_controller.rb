class ApplicationController < ActionController::Base
  protect_from_forgery

  def after_sign_in_path_for(resource)
    if resource.is_a?(User) && session[:link_to]
      where_to = session[:link_to]
      session[:link_to] = nil
      where_to
    else
      super
    end
  end

  def js_redirect_to(path)
    render js: %(window.location.href='#{path}')
  end
end
