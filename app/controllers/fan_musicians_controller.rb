class FanMusiciansController < ApplicationController
  before_filter :authenticate_user!
  
  def create
    fan_musician = FanMusician.new(musician_id: params[:id], fan_id: current_user.id)
    musician = MusicianProfile.find(params[:id])
    if fan_musician.save
      redirect_to :back, notice: "Now you are a Fan of #{musician.name}"
    else
      flash[:error] = 'Something went wrong you, you might already be a Fan' 
      redirect_to :back
    end
  end
end
