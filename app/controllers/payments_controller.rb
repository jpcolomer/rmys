class PaymentsController < ApplicationController
  before_filter :authenticate_user!

  def new
    @user = get_user
    @user_musician_profile = @user.musician_profile
    @url = request.env["HTTP_REFERER"]
    @url ||= user_path(current_user)
    @payment = Payment.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @payment = Payment.new(params[:payment])
    if @payment.save
      @url = params.key?(:redirect) && params[:redirect].empty? ? user_path(current_user) : params[:redirect]
      redirect_to(@url, notice: "You bought #{@payment.amount} credits")
    else
      flash[:error] = 'Something went wrong with the payment'
      @user = get_user
      render action: 'new'
    end
  end

  private
  def get_user
    current_user
  end

end
