class MusicianProfilesController < ApplicationController
  protect_from_forgery
  before_filter :get_musician_profile, except: [:new, :create, :index]
  before_filter :authenticate_user!, except: [:show, :index]

  def index
    @musicians = get_profiles
    respond_to do |format|
      format.html 
      format.json { render json: @musicians}
    end
  end

  def show
    @is_same_user = is_same_user?
    @concerts = @musician_profile.non_performed_concerts
    if user_signed_in?
      @url = session[:first_url] || request.env["HTTP_REFERER"] || musician_profile_path(@musician_profile)
      @tip = Tip.new(user_id: current_user.id, musician_profile_id: @musician_profile.id) 
    end
  end

  def new
    @musician_profile = MusicianProfile.new
    @musician_profile.user = current_user
    respond_to do |format|
      format.html
    end
  end

  def edit
    unless @musician_profile
      render action: "new"
    end
  end

  def create
    if @musician_profile = MusicianProfile.create(params[:musician_profile].merge(user_id: current_user.id))
      redirect_to(@musician_profile)
    else
      if current_user.musician_profile
        flash[:error] = 'You already have a Profile.'
        redirect_to musician_profile_path(current_user.musician_profile)
      else
        render action: "new"
      end
    end
  end


  def update
    respond_to do |format|
      if is_same_user?
        if @musician_profile.update_attributes(params[:musician_profile])
          format.html { redirect_to musician_profile_path(current_user.musician_profile), notice: 'Profile updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @musician_profile.errors, status: :unprocessable_entity }
        end
      else
          flash[:error] = 'You can\'t edit another\'s person profile'
          if current_user.musician_profile
            format.html { redirect_to edit_musician_profile_path(current_user.musician_profile) }
          else
            format.html { redirect_to new_musician_profile_path }
          end
          format.json { render json: @musician_profile.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @musician_profile.destroy if @musician_profile == current_user.musician_profile
    redirect_to root_url
  end

  private
  def get_musician_profile
    @musician_profile = MusicianProfile.find(params[:id])
  end

  def is_same_user?
    user_signed_in? && @musician_profile == current_user.musician_profile
  end

  def get_profiles_from_search(search)
    MusicianProfile.find_by_name_like(search).valid
  end

  def get_profiles
    params.key?(:name) ? get_profiles_from_search(params[:name]) : MusicianProfile.all
  end
end
