class TicketsController < ApplicationController
  before_filter :authenticate_user!

  def new
    @user = current_user
    @user_musician_profile = @user.musician_profile
    @concert = Concert.find(params[:concert_id])
    @ticket = Ticket.new(user_id: @user.id, concert_id: @concert.id)
  end

  def create
    @user = current_user
    @concert = Concert.find(params[:concert_id])
    @ticket = Ticket.new params[:ticket].merge({user_id: @user.id})
    if @ticket.save
      flash[:notice] = "Ticket bought for concert #{@ticket.concert.name}"
      respond_to do |format|
        format.html { redirect_to root_path }
        format.js { js_redirect_to root_path }
      end
    else
      unless @user.has_enough_credit?(@ticket.amount)
        flash[:error] = 'You need to buy more credits to purchase a ticket'
        redirect_to new_payment_path
      else
        render action: 'new'
      end
    end
  end
end
