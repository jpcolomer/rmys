class PendingMusicianValidation < ActiveRecord::Base
  attr_accessible :musician_profile_id

  belongs_to :musician_profile

  def next
    self.class.where("id > ?", id).order("id ASC").first
  end

  def previous
    self.class.where("id < ?", id).order("id DESC").first
  end
end
