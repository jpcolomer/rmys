require './lib/charging.rb'
class Tip < ActiveRecord::Base
  include Charge
  attr_accessible :musician_profile_id, :user_id, :amount

  validates_presence_of :musician_profile_id, :user_id

  validates :amount, presence: true, numericality: { only_integer: true, greater_than: 0 }

  belongs_to :user
  belongs_to :musician_profile
end
