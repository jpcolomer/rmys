class Payment < ActiveRecord::Base
  attr_accessible :amount, :user_id
  belongs_to :user

  validates_presence_of :user
  validates :amount, presence: true, numericality: {only_integer: true, greater_than: 0}

  has_one :balance, through: :user

  before_save :increase_user_credit
  # before_create :increase_user_credit

  def increase_user_credit
    self.balance.increase_credit(amount)
  end
end
