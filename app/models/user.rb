require './lib/name_helper.rb'
class User < ActiveRecord::Base
  extend NameHelpers::SearchMethods
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :name, :photo, :photo_cache, :remove_photo
  # attr_accessible :title, :body
  validates_uniqueness_of :email
  validates_presence_of :email, :name

  has_one :musician_profile, dependent: :destroy
  has_many :challenges, dependent: :destroy
  has_many :fan_musicians, dependent: :destroy, foreign_key: 'fan_id'
  has_many :musicians, through: :fan_musicians
  has_one :balance, dependent: :destroy
  has_many :tickets, dependent: :destroy
  has_many :concerts, through: :tickets, :uniq => true
  has_many :tips
  has_many :payments

  mount_uploader :photo, PhotoUploader

  before_create :build_default_balance

  delegate :credit, to: :balance, prefix: true

  def is_musician?
    musician_profile.present? && musician_profile.is_valid?
  end

  def wallet
    unless @wallet
      musician = musician_profile
      if musician
        @wallet = Wallet.new(balance, musician.balance)
      else
        @wallet = balance
      end
    end
    @wallet
  end


  def has_ticket?(concert)
    concerts.include?(concert) || is_musician? && musician_profile.concerts.include?(concert)
  end

  def has_credit? 
    wallet.credit > 0
  end

  def has_enough_credit?(price = 10)
    wallet.credit >= price.to_i
  end

  def reduce_credit(amount = 10)
    wallet.reduce_credit(amount)
  end

  def increase_credit(amount = 10)
    balance.increase_credit(amount)
  end

  private
  def build_default_balance
    build_balance
    true
  end

end
