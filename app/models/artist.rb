require './lib/name_helper.rb'
class Artist < ActiveRecord::Base
  include NameHelpers
  attr_accessible :name

  has_many :songs, dependent: :destroy
  has_many :challenges, through: :songs, dependent: :destroy

  validates :name, presence: true, uniqueness: true

  def to_s
    name
  end


  def self.create(attributes)
    if attributes.key?(:name) && result = find_by_name(attributes[:name])
      result
    else
      super(attributes)
    end
  end


  def self.save(attributes)
    if attributes.key?(:name) && result = find_by_name(attributes[:name])
      result
    else
      super(attributes)
    end
  end


end
