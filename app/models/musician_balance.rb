require './lib/balance_module.rb'
class MusicianBalance < ActiveRecord::Base
  include BalanceBehaviour
  attr_accessible :musician_profile_id
  belongs_to :musician_profile

  validates :musician_profile_id, presence: true
end
