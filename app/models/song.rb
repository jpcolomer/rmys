require './lib/name_helper.rb'
class Song < ActiveRecord::Base
  include NameHelpers
  attr_accessible :artist_id, :name, :artist_attributes
  
  has_many :challenges, dependent: :destroy
  belongs_to :artist

  validates_presence_of :artist

  validates :name, presence: true, uniqueness: {scope: :artist_id}

  accepts_nested_attributes_for :artist

  def to_s
    name
  end

end
