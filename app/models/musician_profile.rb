require './lib/name_helper.rb'
class MusicianProfile < ActiveRecord::Base
  extend NameHelpers::SearchMethods
  include Rails.application.routes.url_helpers
  attr_accessible :edito, :other_url, :name, :photo, :youtube_url, :user_id, :validation, :photo_cache, :remove_photo, :facebook_url

  belongs_to :user

  has_many :concerts, dependent: :destroy
  has_many :fan_musicians, dependent: :destroy, foreign_key: 'musician_id'
  has_many :fans, through: :fan_musicians
  has_one :pending_musician_validation, dependent: :destroy
  has_many :tips
  has_one :balance, class_name: 'MusicianBalance', dependent: :destroy

  validates :edito, :length => { :maximum => 300, :too_long => "Description is too long, %{count} characters is the maximum allowed" }
  validates_presence_of :user_id, :name
  validates_uniqueness_of :user_id

  validates :youtube_url, format: {with: /(^$|youtube\.com\/watch\?v=.+)/, message: 'invalid, must be a youtube url'}, allow_blank: true
  validates :youtube_url, format: {with: URI::regexp(%w(http https)), message: 'insert the entire url including http'}, allow_blank: true 
  validates :facebook_url, format: {with: /(^$|facebook\.com\/.+)/, message: 'invalid, must be a facebook url'}, allow_blank: true
  validates :facebook_url, format: {with: URI::regexp(%w(http https)), message: 'insert the entire url including http'}, allow_blank: true


  validates :other_url, format: {with: URI::regexp(%w(http https)), message: 'insert the entire url including http'}, allow_blank: true



  mount_uploader :photo, PhotoUploader

  before_create :build_pending_validation, :build_default_balance

  delegate :credit, to: :balance, prefix: true

  scope :valid, where(validation: true)

  def to_s
    name
  end

  def can_play_concert?
    concerts.non_performed.count < 1    
  end

  def has_fan?(user)
    fans.exists?(user)
  end

  def increase_credit(credit)
    balance.credit += credit
    balance.save
  end

  def valid_profile?
    validation
  end

  alias :is_valid? :valid_profile?

  def domain_for_other_url
    URI.parse(other_url).host.gsub(/.*www\./i,'')
  end

  def valid_concerts
    concerts.non_performed.valid_date
  end

  def non_performed_concerts
    concerts.non_performed
  end

  private
  def build_pending_validation
    build_pending_musician_validation
    true
  end

  def build_default_balance
    build_balance
    true
  end


end
