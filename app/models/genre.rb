require './lib/name_helper.rb'

class Genre < ActiveRecord::Base

  GENRES = ['Rock','Pop', "Singer / Songwriter", "Blues & Jazz", "Classical music", 'Shred']

  extend NameHelpers::SearchMethods
  attr_accessible :name
  belongs_to :challenge

  validates :name, presence: true, :inclusion => { :in => GENRES }

  def to_s
    name
  end

end
