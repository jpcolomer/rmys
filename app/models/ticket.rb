require './lib/charging.rb'
class Ticket < ActiveRecord::Base

  attr_accessible :concert_id, :user_id, :amount, :donation
  belongs_to :concert
  belongs_to :user

  validates_presence_of :concert, :user

  include Charge

  validates :amount, presence:true, numericality: { only_integer: true, greater_than_or_equal_to: 10, less_than_or_equal_to: 500 }
  validates :donation, numericality: { only_integer: true,  greater_than: 0 }, :allow_blank => true

  validates :concert_id, uniqueness: {scope: :user_id, message: 'ticket already bought'}

  before_validation :set_amount

  def musician_profile
    concert.musician_profile
  end


  def set_amount
    self.amount = 10 + (donation || 0)
  end

end
