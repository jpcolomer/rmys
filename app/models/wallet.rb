class Wallet
  attr_accessor :user_balance, :musician_balance

  def initialize(user_balance, musician_balance=nil)
    @user_balance = user_balance
    @musician_balance = musician_balance
  end

  NilClass.class_eval {def credit; 0; end}

  def credit
    @user_balance.credit + @musician_balance.credit
  end

  def has_credit?
    credit > 0
  end

  def has_enough_credit?(price)
    credit >= price.to_i
  end

  def reduce_credit(amount)
    if @user_balance.credit >= amount
      reduce_only_user_balance(amount)
    elsif has_enough_credit?(amount)
      reduce_user_and_musician_balance(amount)
    else
      nil
    end
  end

  private
  def reduce_only_user_balance(amount)
    @user_balance.credit -= amount
    @user_balance.save
  end

  def reduce_user_and_musician_balance(amount)
    rest = amount - @user_balance.credit
    @user_balance.credit = 0
    @musician_balance.credit -= rest
    @user_balance.save
    @musician_balance.save
  end

end