class Concert < ActiveRecord::Base
  attr_accessible :challenge_id, :date, :description, :name, :performed, :ticket_price, :musician_profile_id, :duration, :end_date

  belongs_to :challenge
  belongs_to :musician_profile

  has_many :tickets, dependent: :destroy
  has_many :users, through: :tickets

  validates_presence_of :date, :name, :musician_profile, :challenge, :duration
  validates_numericality_of :duration

  validates :description, :length => { :maximum => 300 }

  validates :musician_profile_id, uniqueness: {scope: :performed, message: "already have one concert to be performed", unless: lambda{|concert| concert.performed} }
  validate :duration, :inclusion => { :in => [10, 30], :message => "%{value} is not a valid duration" }
  # validate :musician_has_more_than_one_non_performed_concert
  validate :date_is_less_than_now, on: :create

  scope :non_performed, where(performed: false)
  scope :recent, order('id desc')
  scope :greater_date, lambda { |datetime = DateTime.now| where("date > ?" , datetime) }
  scope :less_date, lambda { |datetime = DateTime.now| where("date < ?", datetime)}
  scope :closest, order('date asc')
  scope :valid_date, lambda {where("end_date > ?", DateTime.now)}

  delegate :song, :artist, to: :challenge
  delegate :balance, to: :musician_profile, :prefix => :musician

  before_save do
    self.end_date =  self.date + self.duration.minutes
  end

  alias :musician :musician_profile

  def pretty_date
    date.strftime("%d/%m/%Y %I:%M%p")
  end


  class << self
    def closest_concert
      valid_date.non_performed.closest.first
    end

    def farest_concert
      valid_date.non_performed.closest.last
    end

  end

  def next
    self.class.greater_date(date).valid_date.non_performed.closest.first
  end

  def previous
    self.class.less_date(date).valid_date.non_performed.closest.last
  end

  def countdown
    now = DateTime.now
    diff = now.difference_in_days_hours_mins(date)
    '%.2d days, %.2d hours, %.2d minutes' % [diff.days, diff.hours , diff.minutes]
  end

  def musician_has_more_than_one_non_performed_concert
    if musician_profile.concerts.non_performed.size > 1
      errors.add(:performed, ": Musician can't have more than one concert non performed")
    end
  end




  def genres
    challenge.genres_list
  end

  private
  def date_is_less_than_now
    if date < DateTime.now
      errors.add(:date, "can't be less than now")
    end
  end


end
