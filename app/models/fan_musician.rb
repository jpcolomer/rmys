class FanMusician < ActiveRecord::Base
  attr_accessible :musician_id, :fan_id
  belongs_to :musician, class_name: "MusicianProfile"
  belongs_to :fan, class_name: "User"

  validates_presence_of :musician_id, :fan_id
  validates_uniqueness_of :fan_id, :scope => :musician_id
end
