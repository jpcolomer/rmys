require './lib/balance_module.rb'
class Balance < ActiveRecord::Base
  include BalanceBehaviour
  attr_accessible :user_id

  validates_presence_of :user_id
  belongs_to :user

end
