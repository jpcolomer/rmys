class Challenge < ActiveRecord::Base
  attr_accessible :song_id, :user_id, :song_attributes, :genres_attributes, :artist_attributes
  belongs_to :user
  belongs_to :song

  has_many :concerts, dependent: :destroy
  has_many :genres, dependent: :destroy

  validates_presence_of :user, :song, :genres

  delegate :artist, to: :song


  accepts_nested_attributes_for :genres
  accepts_nested_attributes_for :song

  scope :joined_song_artist, joins(song: :artist)
  scope :joined_genres, joins(:genres)
  scope :grouped_by_id, group('challenges.id')
  scope :joined_relations, joined_song_artist.joined_genres

  def artist_name
    artist.name
  end

  def song_name
    song.name
  end

  def genres_list
    genres.join(', ')
  end

  class << self

    def create_with_attributes(attributes)

      ### HAVE TO MAKE NESTED ATTRIBUTES TO WORK WITH THE CONDITION IF AN ARTIST OR SONG ALREADY EXIST, 
      ### RETURN THE ARTIST OR SONG WITHOUT CREATING IT. MAYBE SHOULD USE A PRESENTER
      if attributes.key?(:song_attributes)
        song_attributes = attributes.delete(:song_attributes)
        
        if song_attributes.key?(:artist_attributes)
          artist = Artist.create(song_attributes.delete(:artist_attributes))
        end

        if song_attributes.key?(:name) && song = artist.songs.find_by_name(song_attributes[:name])
          song
        elsif artist && artist.valid?
          song = artist.songs.create(song_attributes)
        end

      end

      if artist && song && artist.valid? && song.valid?
        attributes = attributes.merge(song_id: song.id)
      end

      challenge = new(attributes)
      result = challenge.save
      [result, challenge]
    end
  end


end
