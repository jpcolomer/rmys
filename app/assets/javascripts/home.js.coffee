# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(".typeahead-genre").typeahead
  source: (query, process) ->
    $.getJSON "/genres.json", {name: query}, (json) ->
      process(json)

$(".typeahead-artist").typeahead
  source: (query, process) ->
    $.getJSON "/artists.json", {name: query}, (json) ->
      process(json)

$(".typeahead-song").typeahead
  source: (query, process) ->
    $.getJSON "/songs.json", {name: query}, (json) ->
      process(json)

window.goToMusicianProfile = (id) ->
  window.location.replace("/musician_profiles/#{id}")

$(".typeahead-user-profile").typeahead
  # highlighter: (item) ->
  #   regex = new RegExp( '(' + this.query + ')', 'gi' )
  #   item_str = item.replace( regex, "<strong>$1</strong>" )
  #   "<div onclick='goToMusicianProfile(#{item.user_id})' data-id='#{item.user_id}'' >#{item_str}</div>"   

  updater: (item) ->
    goToMusicianProfile(item.id)

  source: (query, process) ->
    $.getJSON "/musician_profiles.json", {name: query}, (json) ->
      process(for user in json
        item = new String "#{user.name}"
        item.user_id = user.id
        item)


$(document).ready ->

  $('.sign-in-required').click (event) ->
    $('.link_to').attr('value',$(@).data('link-to'))



  $('.anchorLink').click (event) ->
    event.preventDefault()
    $(window).off('scroll')
    $('.chosen_nav').removeClass('chosen_nav')
    elementClick = $(@).attr('href')
    first_char = elementClick[0]
    if first_char == '/'
      window.location.href = elementClick
    else
      destination = $("#{elementClick}").offset().top
      $("html:not(:animated),body:not(:animated)").animate({ scrollTop: destination}, 1100, ->
        window.location.hash = elementClick
        changeNavClassFromScrolling())
      $(@).parent('li').addClass('chosen_nav')
      false

  setNavFromPosition = (position) -> 
    if 620 < position < 1450
      $('.chosen_nav').removeClass('chosen_nav')
      $('.anchorLink').parent('li:first').addClass('chosen_nav')
    else if position >= 1450
      $('.chosen_nav').removeClass('chosen_nav')
      $('.anchorLink').parent('li:nth-child(2)').addClass('chosen_nav')
    else
      $('.chosen_nav').removeClass('chosen_nav')

  changeNavClassFromScrolling = ->
    if window.location.pathname is '/'
      $(window).on('scroll', ->
        position = window.pageYOffset
        setNavFromPosition(position)
      )

  changeNavClassFromScrolling()
