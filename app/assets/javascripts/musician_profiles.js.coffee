# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$('#musician_profile_photo_btn').click (e)->
  e.preventDefault()
  $('#musician_profile_photo').click()

$('.photo_form #musician_profile_photo').change (e) ->
  $('form').submit()


$(document).ready( ->
  window.readURL = (input) ->
    if input.files && input.files[0]
      reader = new FileReader()
      reader.onload = (e) ->
        $('.thumbnails li img').attr('src', e.target.result)

      reader.readAsDataURL(input.files[0])
)