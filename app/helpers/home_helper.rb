module HomeHelper

  def front_page_path(options={})
    if params[:controller] == 'home'
      "\##{options[:anchor]}"
    else
      root_path(options)
    end
  end
end
