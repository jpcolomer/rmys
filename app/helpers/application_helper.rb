module ApplicationHelper


  def sortable(column, title = nil)
    title ||= column.titleize
    direction = (column == sort_column.to_s && sort_direction == "asc") ? "desc" : "asc"
    link_to title, :sort => column, :direction => direction
  end

  def css_class(column)
    (column == sort_column.to_s) ? "#{sort_direction}" : nil
  end


end
