module ChallengesHelper

  def perform_path
    if user_signed_in?
      challenges_path
    else
      # session[:link_to] = challenges_path
      '#log-in-modal'
    end
  end

  def perform_link
    if user_signed_in?
      link_to 'Perform Live Online', challenges_path
    else
      link_to 'Perform Live Online', '#log-in-modal', data: {toggle: 'modal'}
    end
  end

end
