module BroadcastHelper
  def show_concert_broadcast_path(concert)
    if user_signed_in?
      concert_broadcast_path(concert)
    else
      '#log-in-modal'
    end
  end
end
