module TipsHelper
  def give_tip_path(musician)
    if user_signed_in?
      '#give-tip-modal'
    else
      '#log-in-modal'
    end
  end
end
