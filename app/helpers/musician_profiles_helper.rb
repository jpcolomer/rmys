module MusicianProfilesHelper
  def other_url_tag(asset)
    tag = asset
    tag = image_tag("#{asset}.png") if Rails.application.assets.find_asset("#{asset}.png")
    tag
  end

  def add_fb_like
    "<fb:like send='true' width='450' show_faces='true' colorscheme='dark'></fb:like>".html_safe
  end
end
