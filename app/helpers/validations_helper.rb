module ValidationsHelper
  def accept_path(pending_profile)
    validations_path(musician_profile_id: pending_profile.musician_profile.id, musician_profile: {validation: true})
  end

  def reject_path(pending_profile)
    validations_path(musician_profile_id: pending_profile.musician_profile.id, musician_profile: {validation: false})
  end

  def pending_validations_path(pending_profile)
    if pending_profile
      validation_path(pending_profile)
    else
      '#'
    end
  end
end
