module ConcertsHelper
  def on_air_path(concert)
    if concert
      concert_path(concert)
    else
      nil
    end
  end

  def buy_ticket_path(concert)
    if user_signed_in?
      new_concert_ticket_path(concert)
    else
      '#log-in-modal'
    end
  end
end
